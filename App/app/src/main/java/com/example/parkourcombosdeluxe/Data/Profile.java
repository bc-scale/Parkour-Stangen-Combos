package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Profile {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    @NonNull
    public String name;
    public String description;
    //Settings
    public Integer currentSpotId;
    public Integer currentGameMode;
    public Integer amountMovesBase;
    public Integer amountMovesFuzzMinus;
    public Integer amountMovesFuzzPlus;
    public Integer amountMovesBoundBottom;
    public Integer amountMovesBoundTop;
    public Boolean amountMovesBoundSetable;
    public String defaultMainLanguageForNewObjectTexts;
    public Boolean editEnglishNames;
    public Boolean editGermanNames;
    public Integer currentColorTheme;
    public Integer colorCodingMoveCardsMode;
    public Integer sortByMoveEditMode;
    public Integer sortByMoveSelectMode;
    public Boolean sortMoveEditAscending;
    public Boolean sortMoveSelectAscending;
    public Boolean tippsEnabled;



    static public final int GAME_MODE_NORMAL = 0;
    static public final int GAME_MODE_BAGPACKING = 1;
    static public final int GAME_MODE_BAGPACKING_ROTATION = 2;

    static public final int COLOR_THEME_STANDARD = 0;
    static public final int COLOR_THEME_LION = 1;
    static public final int COLOR_THEME_EF_DARK = 2;
    static public final int COLOR_THEME_EF_LIGHT = 3;
    static public final int COLOR_THEME_FDROID_DARK = 4;
    static public final int COLOR_THEME_ANDROID = 5;
    static public final int COLOR_THEME_UBUNTU_TUX = 6;
    static public final int COLOR_THEME_K9_RED = 7;
    static public final int COLOR_THEME_AUXCASTLE_TRADITIONAL = 8;
    static public final int COLOR_THEME_SIMPLE_ORANGE = 9;
    static public final int COLOR_THEME_CMY_LIGHT = 10;
    static public final int COLOR_THEME_RGB_LIGHT = 11;
    static public final int COLOR_THEME_MONOCHROME = 12;
    static public final int COLOR_THEME_GOOGLE = 13;
    static public final int COLOR_THEME_GOOGLE_PLAY = 14;

    static public final int MOVE_CARDS_COLOR_EMPTY = 0;
    static public final int MOVE_CARDS_COLOR_LEFT = 1;
    static public final int MOVE_CARDS_COLOR_RIGHT = 2;
    static public final int MOVE_CARDS_COLOR_DOUBLE = 3;

    static public final int SORT_BY_SELECTED = 0;
    static public final int SORT_BY_DATE = 1;
    static public final int SORT_BY_NAME = 2;
    static public final int SORT_BY_FAVORITE = 3;
    static public final int SORT_BY_DIFFICULTY = 4;
    static public final int SORT_BY_SPOTTYPE = 5;



    public Profile(@NonNull String name, String description) {
        this.name = name;
        this.description = description;
        this.currentSpotId = null;
        this.currentGameMode = 0;
        this.amountMovesBase = 5;
        this.amountMovesFuzzMinus = 1;
        this.amountMovesFuzzPlus = 1;
        this.amountMovesBoundBottom = 3;
        this.amountMovesBoundTop = 6;
        this.amountMovesBoundSetable = false;
        this.defaultMainLanguageForNewObjectTexts = null;
        this.editEnglishNames = false;
        this.editGermanNames = false;
        this.currentColorTheme = 0;
        this.colorCodingMoveCardsMode = 0;
        this.sortByMoveEditMode = 1;
        this.sortByMoveSelectMode = 1;
        this.sortMoveEditAscending = true;
        this.sortMoveSelectAscending = true;
        this.tippsEnabled = true;
    }

    @Ignore
    public Profile(@NonNull String name, String description, Boolean editEnglishNames, Boolean editGermanNames) {
        this.name = name;
        this.description = description;
        this.currentSpotId = null;
        this.currentGameMode = 0;
        this.amountMovesBase = 5;
        this.amountMovesFuzzMinus = 1;
        this.amountMovesFuzzPlus = 1;
        this.amountMovesBoundBottom = 3;
        this.amountMovesBoundTop = 6;
        this.amountMovesBoundSetable = false;
        this.colorCodingMoveCardsMode = 0;
        this.editEnglishNames = editEnglishNames;
        this.editGermanNames = editGermanNames;
        this.currentColorTheme = 0;
        this.defaultMainLanguageForNewObjectTexts = null;
        this.sortByMoveEditMode = 1;
        this.sortByMoveSelectMode = 1;
        this.sortMoveEditAscending = true;
        this.sortMoveSelectAscending = true;
        this.tippsEnabled = true;
    }

    @Ignore
    public Profile(@NonNull String name) {
        this.name = name;
        this.description = "";
        this.currentSpotId = null;
        this.currentGameMode = 0;
        this.amountMovesBase = 5;
        this.amountMovesFuzzMinus = 1;
        this.amountMovesFuzzPlus = 1;
        this.amountMovesBoundBottom = 3;
        this.amountMovesBoundTop = 6;
        this.amountMovesBoundSetable = false;
        this.colorCodingMoveCardsMode = 0;
        this.editEnglishNames = false;
        this.editGermanNames = false;
        this.currentColorTheme = 0;
        this.defaultMainLanguageForNewObjectTexts = null;
        this.sortByMoveEditMode = 1;
        this.sortByMoveSelectMode = 1;
        this.sortMoveEditAscending = true;
        this.sortMoveSelectAscending = true;
        this.tippsEnabled = true;
    }

    @Ignore
    public Profile(@NonNull String name, String description, Integer currentSpotId) {
        this.name = name;
        this.description = description;
        this.currentSpotId = currentSpotId;
        this.currentGameMode = 0;
        this.amountMovesBase = 5;
        this.amountMovesFuzzMinus = 1;
        this.amountMovesFuzzPlus = 1;
        this.amountMovesBoundBottom = 3;
        this.amountMovesBoundTop = 6;
        this.amountMovesBoundSetable = false;
        this.colorCodingMoveCardsMode = 0;
        this.editEnglishNames = false;
        this.editGermanNames = false;
        this.currentColorTheme = 0;
        this.defaultMainLanguageForNewObjectTexts = null;
        this.sortByMoveEditMode = 1;
        this.sortByMoveSelectMode = 1;
        this.sortMoveEditAscending = true;
        this.sortMoveSelectAscending = true;
        this.tippsEnabled = true;
    }

    @Ignore
    public Profile(@NonNull String name, Integer currentSpotId) {
        this.name = name;
        this.description = "";
        this.currentSpotId = currentSpotId;
        this.currentGameMode = 0;
        this.amountMovesBase = 5;
        this.amountMovesFuzzMinus = 1;
        this.amountMovesFuzzPlus = 1;
        this.amountMovesBoundBottom = 3;
        this.amountMovesBoundTop = 6;
        this.amountMovesBoundSetable = false;
        this.colorCodingMoveCardsMode = 0;
        this.editEnglishNames = false;
        this.editGermanNames = false;
        this.currentColorTheme = 0;
        this.defaultMainLanguageForNewObjectTexts = null;
        this.sortByMoveEditMode = 1;
        this.sortByMoveSelectMode = 1;
        this.sortMoveEditAscending = true;
        this.sortMoveSelectAscending = true;
        this.tippsEnabled = true;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    @NonNull
    public String getName() {
        return name;
    }
    public void setName(@NonNull String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public Integer getCurrentSpotId() {
        return currentSpotId;
    }
    public void setCurrentSpotId(Integer currentSpotId) {
        this.currentSpotId = currentSpotId;
    }

    public Integer getCurrentGameMode() {
        return currentGameMode;
    }
    public void setCurrentGameMode(Integer currentGameMode) {
        this.currentGameMode = currentGameMode;
    }

    public Integer getAmountMovesBase() { return amountMovesBase; }
    public void setAmountMovesBase(Integer amountMovesBase) {
        this.amountMovesBase = amountMovesBase;
    }

    public Integer getAmountMovesFuzzMinus() { return amountMovesFuzzMinus; }
    public void setAmountMovesFuzzMinus(Integer amountMovesFuzzMinus) {
        this.amountMovesFuzzMinus = amountMovesFuzzMinus;
    }

    public Integer getAmountMovesFuzzPlus() { return amountMovesFuzzPlus; }
    public void setAmountMovesFuzzPlus(Integer amountMovesFuzzPlus) {
        this.amountMovesFuzzPlus = amountMovesFuzzPlus;
    }

    public Integer getAmountMovesBoundBottom() { return amountMovesBoundBottom; }
    public void setAmountMovesBoundBottom(Integer amountMovesBoundBottom) {
        this.amountMovesBoundBottom = amountMovesBoundBottom;
    }

    public Integer getAmountMovesBoundTop() { return amountMovesBoundTop; }
    public void setAmountMovesBoundTop(Integer amountMovesBoundTop) {
        this.amountMovesBoundTop = amountMovesBoundTop;
    }

    public Boolean getAmountMovesBoundSetable() { return amountMovesBoundSetable; }
    public void setAmountMovesBoundSetable(Boolean amountMovesBoundSetable) {
        this.amountMovesBoundSetable = amountMovesBoundSetable;
    }

    public Integer getColorCodingMoveCardsMode() {
        return colorCodingMoveCardsMode;
    }
    public void setColorCodingMoveCardsMode(Integer colorCodingMoveCardsMode) {
        this.colorCodingMoveCardsMode = colorCodingMoveCardsMode;
    }

    public Boolean getEditEnglishNames() {
        return editEnglishNames;
    }
    public void setEditEnglishNames(Boolean editEnglishNames) {
        this.editEnglishNames = editEnglishNames;
    }

    public Boolean getEditGermanNames() {
        return editGermanNames;
    }
    public void setEditGermanNames(Boolean editGermanNames) {
        this.editGermanNames = editGermanNames;
    }

    public Integer getCurrentColorTheme() { return currentColorTheme; }
    public void setCurrentColorTheme(Integer currentColorTheme) {
        this.currentColorTheme = currentColorTheme;
    }

    public String getDefaultMainLanguageForNewObjectTexts() {
        return defaultMainLanguageForNewObjectTexts;
    }
    public void setDefaultMainLanguageForNewObjectTexts(String defaultMainLanguageForNewObjectTexts) {
        this.defaultMainLanguageForNewObjectTexts = defaultMainLanguageForNewObjectTexts;
    }

    public Integer getSortByMoveEditMode() {
        return sortByMoveEditMode;
    }
    public void setSortByMoveEditMode(Integer sortByMoveEditMode) {
        this.sortByMoveEditMode = sortByMoveEditMode;
    }

    public Integer getSortByMoveSelectMode() {
        return sortByMoveSelectMode;
    }
    public void setSortByMoveSelectMode(Integer sortByMoveSelectMode) {
        this.sortByMoveSelectMode = sortByMoveSelectMode;
    }

    public Boolean getSortMoveEditAscending() {
        return sortMoveEditAscending;
    }
    public void setSortMoveEditAscending(Boolean sortMoveEditAscending) {
        this.sortMoveEditAscending = sortMoveEditAscending;
    }

    public Boolean getSortMoveSelectAscending() {
        return sortMoveSelectAscending;
    }
    public void setSortMoveSelectAscending(Boolean sortMoveSelectAscending) {
        this.sortMoveSelectAscending = sortMoveSelectAscending;
    }

    public Boolean getTippsEnabled() { return tippsEnabled; }
    public void setTippsEnabled(Boolean tippsEnabled) {
        this.tippsEnabled = tippsEnabled;
    }
}