package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ProfileDao {

    //TODO: (here and everywhere else): make this sort by ID, if necessary
    @Query("SELECT * FROM profile")
    List<Profile> getAll();

    //TODO: (here and everywhere else): make this sort by ID, if necessary
    @Query("SELECT ID, name, description FROM profile")
    List<ProfileMinimal> getAllMinimal();

    @Query("SELECT ID FROM profile")
    List<Integer> getAllIDs();

    @Query("SELECT * FROM profile where ID LIKE  :ID")
    Profile findByID(Integer ID);

    @Insert
    void insertAll(Profile... profiles);

    @Update
    void updateProfiles(Profile... profiles);

    @Delete
    void delete(Profile profile);

    @Query("DELETE FROM profile")
    void deleteAll();

/*    @Query("SELECT * FROM user WHERE age > :minAge")
    public User[] loadAllUsersOlderThan(int minAge);

    @Query("SELECT * FROM user WHERE age BETWEEN :minAge AND :maxAge")
    public User[] loadAllUsersBetweenAges(int minAge, int maxAge);

    @Query("SELECT * FROM user WHERE first_name LIKE :search " +
           "OR last_name LIKE :search")
    public List<User> findUserWithName(String search);

    @Query("SELECT * FROM book " +
           "INNER JOIN loan ON loan.book_id = book.id " +
           "INNER JOIN user ON user.id = loan.user_id " +
           "WHERE user.name LIKE :userName")
    public List<Book> findBooksBorrowedByNameSync(String userName);*/
}