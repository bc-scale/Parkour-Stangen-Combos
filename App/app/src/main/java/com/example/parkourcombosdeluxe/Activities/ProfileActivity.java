package com.example.parkourcombosdeluxe.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.ProfileDao;
import com.example.parkourcombosdeluxe.Data.Recommendation;
import com.example.parkourcombosdeluxe.Data.RecommendationDao;
import com.example.parkourcombosdeluxe.Data.RecommendationProfileJoin;
import com.example.parkourcombosdeluxe.Data.RecommendationProfileJoinDao;
import com.example.parkourcombosdeluxe.Data.Spot;
import com.example.parkourcombosdeluxe.Data.SpotDao;
import com.example.parkourcombosdeluxe.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.layout_ProfileScroll)
    LinearLayout layout_ProfileScroll;
    /*@BindView(R.id.action_profiles_profile_add)
    LinearLayout action_profiles_profile_add;
    @BindView(R.id.action_profiles_help)
    LinearLayout action_profiles_help;*/


    String tempProfileNameFromDiscard = "";
    String tempProfileDescrFromDiscard = "";
    Boolean newProfileDiscarded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recommendationProfileJoinDao = getDBs.recommendationProfileJoinDao();
        profileDao = getDBs.profileDao();
        recommendationDao = getDBs.recommendationDao();
        generateProfile();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
/*            case android.R.id.home:
                finishWithResultOk();
                break;*/
            case R.id.action_profiles_profile_add:
                createDialogAddProfile(ProfileActivity.this);
                break;
            case R.id.action_profiles_help:
                createDialogHelpBasic(ProfileActivity.this, getResources().getString(R.string.profiles_dialog_help_title), getResources().getString(R.string.profiles_dialog_help_text));
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }






    private void generateProfile() {
        List<Profile> profiles = profileDao.getAll();
        final int profilesSize = profiles.size();
        //final Profile currentProfile = profileDao.findByID(sharedPref.getInt(getResources().getString(R.string.sharedpref_current_profileid), 1));
        final int currentProfileId = currentProfile.getID();
        layout_ProfileScroll.removeAllViews();

        TextView statusTopTextView = new TextView(ProfileActivity.this);
        statusTopTextView.setText(getResources().getString(R.string.profiles_title_status) + " " +
                currentProfile.getName() );
        statusTopTextView.setTextColor(getResources().getColor(R.color.colorTextLight));
        statusTopTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_title));
        statusTopTextView.setTypeface(Typeface.DEFAULT_BOLD);
        layout_ProfileScroll.addView(statusTopTextView);
        /*ImageView v = new ImageView(ProfileActivity.this);
        v.setImageDrawable(getResources().getDrawable(R.style.Divider));
        layout_ProfileScroll.addView(v);*/

        //BE CAREFUL: i should be one smaller than profiles.size() in the last loop, as i is used in profiles.get(i), where counting starts from 0
        for (int i = 0; i < profiles.size(); i++) {
            //ALTER give the textview margin within a layout
            LinearLayout.LayoutParams paramsDescription = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            paramsDescription.leftMargin = 30;
            LinearLayout.LayoutParams paramsEdit = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //TODO: get exact size needed (cheese)
            paramsEdit.height = (int) (getResources().getDimensionPixelSize(R.dimen.scrollview_title) * 1.5);

            final Profile nextProfile = profiles.get(i);

            LinearLayout profileEntryWhole = new LinearLayout(ProfileActivity.this);
            profileEntryWhole.setOrientation(LinearLayout.VERTICAL);

            LinearLayout upperHalf = new LinearLayout(ProfileActivity.this);

            LinearLayout lowerHalf = new LinearLayout(ProfileActivity.this);
            //lowerHalf.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));

            LinearLayout withProfile = new LinearLayout(ProfileActivity.this);
            withProfile.setOrientation(LinearLayout.VERTICAL);

            TextView profileNameText = new TextView(ProfileActivity.this);
            profileNameText.setText(nextProfile.getName());
            profileNameText.setTextColor(getResources().getColor(R.color.colorTextLight));
            profileNameText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_title));
            profileNameText.setTypeface(Typeface.DEFAULT_BOLD);
            if (currentProfileId == nextProfile.getID()) {
                profileNameText.setClickable(false);
            } else {
                profileNameText.setClickable(true);
                profileNameText.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        createDialogSelectProfile(nextProfile.getName(), nextProfile.getID(), ProfileActivity.this);
                    }

                });
            }

            //ALTER makes the rest of the views that share space in a Layout with this view go to the edge of the Layout
            profileNameText.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1.0f));

            ImageButton editProfile = new ImageButton(ProfileActivity.this);
            editProfile.setImageDrawable(getResources().getDrawable(R.drawable.iconn_pen_edit_diagonal));
            editProfile.setColorFilter(getResources().getColor(R.color.colorButtonLight));
            editProfile.setId(nextProfile.getID());
            editProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
            editProfile.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    createDialogEditProfile(view, ProfileActivity.this);
                }

            });


            ImageView profileHead = new ImageView(ProfileActivity.this);
            profileHead.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_profile));
            profileHead.setColorFilter(getResources().getColor(R.color.colorTurquoiseProfile));

            ImageButton deleteProfileTrashBin = new ImageButton(ProfileActivity.this);
            deleteProfileTrashBin.setImageDrawable(getResources().getDrawable(R.drawable.ic_trash_bin));
            deleteProfileTrashBin.setColorFilter(getResources().getColor(R.color.colorButtonLight));
            deleteProfileTrashBin.setId(nextProfile.getID());
            deleteProfileTrashBin.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (profilesSize < 2) {
                        toastMessage(getResources().getString(R.string.toast_profile_delete_failed_to_few_profiles));
                    } else {
                        createDialogDeleteProfile(view, ProfileActivity.this);
                    }
                }

            });

            TextView profileDescriptionText = new TextView(ProfileActivity.this);
            if (nextProfile.getDescription().isEmpty()) {
                profileDescriptionText.setText(getResources().getString(R.string.profiles_no_description_given));
            } else {
                profileDescriptionText.setText(nextProfile.getDescription());
            }
            profileDescriptionText.setTextColor(getResources().getColor(R.color.colorTextLight));
            profileDescriptionText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_descr));
            if (currentProfileId == nextProfile.getID()) {
                profileDescriptionText.setClickable(false);
            } else {
                profileDescriptionText.setClickable(true);
                profileDescriptionText.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        createDialogSelectProfile(nextProfile.getName(), nextProfile.getID(), ProfileActivity.this);
                    }

                });
            }


            //add views
            upperHalf.addView(profileNameText);
            upperHalf.addView(editProfile, paramsEdit);

            withProfile.addView(profileHead);
            withProfile.addView(deleteProfileTrashBin);

            lowerHalf.addView(withProfile);
            lowerHalf.addView(profileDescriptionText, paramsDescription);

            profileEntryWhole.addView(upperHalf);
            profileEntryWhole.addView(lowerHalf);

            layout_ProfileScroll.addView(profileEntryWhole);
        }

    }




    public void createDialogSelectProfile(String profileName, int profileID, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String tempProfileName = profileName;
        final int tempProfileID = profileID;
        String titleMessage = getString(R.string.dialog_select_profile_title1) + tempProfileName + getString(R.string.dialog_select_profile_title2);
        builder.setTitle(titleMessage);
        builder.setPositiveButton(getString(R.string.dialog_button_confirm_normal), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.sharedpref_current_profileid), tempProfileID);
                editor.commit();
                makeMainRecreate();
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_button_decline_normal), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }


    private void createDialogDeleteProfile(View view, Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final int tempID = view.getId();
        final Profile tempProfile = profileDao.findByID(tempID);

        final TextView deleteTitle = (TextView) layout.findViewById(R.id.warning_dialog_title);
        final TextView deleteDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);

        deleteDescr.setText(getString(R.string.dialog_delete_entry) + " " + tempProfile.getName());

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recommendationProfileJoinDao.deleteWithProfileId(tempProfile.getID());
                profileDao.delete(tempProfile);

                // delete all spots of this profile
                SpotDao spotDao = AppDatabase.getInstance(ProfileActivity.this).spotDao();
                List <Spot> currentProfileSpots = spotDao.getAllByProfileID(tempProfile.getID());
                if (currentProfileSpots != null) {
                    for (int i = 0; i < currentProfileSpots.size(); i++) {
                        spotDao.delete(currentProfileSpots.get(i));
                    }
                }

                toastMessage(getString(R.string.toast_confirm_delete_entry));
                currentProfile = getCurrentProfile();

                makeMainRecreate();
                alertDialog.dismiss();
                recreate();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }




    //TODO: add judgements to certain names
    private void createDialogEditProfile(View view, Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_edit_profile, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final Context contextFinal = context;
        final int tempID = view.getId();
        final Profile tempProfile = profileDao.findByID(tempID);

        final TextView editTitle = (TextView) layout.findViewById(R.id.edit_profile_dialog_title);
        final EditText editName = (EditText) layout.findViewById(R.id.edit_profile_dialog_name);
        final EditText editDescr = (EditText) layout.findViewById(R.id.edit_profile_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.edit_profile_dialog_button_save);
        Button cancelButton = (Button) layout.findViewById(R.id.edit_profile_dialog_button_cancel);

        editTitle.setText(getString(R.string.dialog_edit_profile_name_title));
        editName.setText(tempProfile.getName());
        editDescr.setText(tempProfile.getDescription());

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String newEditName = editName.getText().toString();
                final String newEditDescr = editDescr.getText().toString();

                if (newEditName.isEmpty()) {
                    toastMessage(getResources().getString(R.string.toast_profile_name_empty));
                } else {
                    if (checkDoubleProfileNamesExcludingSelf(tempProfile, newEditName)) {
                        createDialogEditProfileDoubleWarning(tempProfile, newEditName, newEditDescr, contextFinal);
                    } else {
                        tempProfile.setName(newEditName);
                        tempProfile.setDescription(newEditDescr);

                        profileDao.updateProfiles(tempProfile);

                        toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                        makeMainRecreate();
                        alertDialog.dismiss();
                        recreate();
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    private Boolean checkDoubleProfileNamesExcludingSelf(Profile excludedProfile, String checkProfileName) {
        List<Profile> checkProfiles = profileDao.getAll();
        for (Profile a : checkProfiles) {
            if (a.getName().equalsIgnoreCase(checkProfileName)) {
                if (a.getID() != excludedProfile.getID()) {
                    return true;
                }
            }
        }
        return false;
    }

    private void createDialogEditProfileDoubleWarning(final Profile toBeChangedProfile, String doubleName, String descr, final Context context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final String newProfileName = doubleName;
        final String newProfiledescr = descr;

        final TextView deleteTitle = (TextView) layout.findViewById(R.id.warning_dialog_title);
        final TextView deleteDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);

        String message = getResources().getString(R.string.dialog_profileedit_double_name_conflict_1)
                + toBeChangedProfile.getName() + getResources().getString(R.string.dialog_profileedit_double_name_conflict_2)
                + doubleName + getResources().getString(R.string.dialog_profileedit_double_name_conflict_3);
        deleteDescr.setText(message);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toBeChangedProfile.setName(newProfileName);
                toBeChangedProfile.setDescription(newProfiledescr);

                profileDao.updateProfiles(toBeChangedProfile);

                toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                makeMainRecreate();
                alertDialog.dismiss();
                recreate();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }




    //TODO ask to copy current Spot (from profile currently used) to new Profile after creating new profile
    private void createDialogAddProfile(Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_create_twoliner, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final TextView addTitle = (TextView) layout.findViewById(R.id.add_twoliner_dialog_title);
        final EditText newName = (EditText) layout.findViewById(R.id.add_twoliner_dialog_name);
        final EditText newDescr = (EditText) layout.findViewById(R.id.add_twoliner_dialog_descr);
        Button saveButton = (Button) layout.findViewById(R.id.add_twoliner_dialog_button_save);
        Button cancelButton = (Button) layout.findViewById(R.id.add_twoliner_dialog_button_cancel);

        addTitle.setText(getResources().getString(R.string.dialog_create_profile_title));
        if (!(tempProfileNameFromDiscard.equals("") && tempProfileDescrFromDiscard.equals(""))) {
            newName.setText(tempProfileNameFromDiscard);
            newDescr.setText(tempProfileDescrFromDiscard);
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String newProfileName = newName.getText().toString();
                final String newProfileDescr;
                if (newDescr.getText().toString() == null) {
                    newProfileDescr = "";
                } else {
                    newProfileDescr = newDescr.getText().toString();
                }

                if (newProfileName.isEmpty()) {
                    toastMessage(getResources().getString(R.string.toast_profile_name_empty));
                } else {
                    if (checkDoubleProfileNames(newProfileName)) {
                        createDialogAddProfileDoubleWarning(newProfileName, newProfileDescr, ProfileActivity.this);
                    } else {
                        Profile newProfile = new Profile(newProfileName, newProfileDescr);
                        profileDao.insertAll(newProfile);
                        joinNewProfileWithAllRecommendations();

                        tempProfileNameFromDiscard = "";
                        tempProfileDescrFromDiscard = "";

                        String message = getResources().getString(R.string.toast_confirm_profile_creation1) + newProfileName + getResources().getString(R.string.toast_confirm_profile_creation2);
                        toastMessage(message);
                        alertDialog.dismiss();
                        ProfileActivity.this.recreate();
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private Boolean checkDoubleProfileNames(String checkProfileName) {
        List<Profile> checkProfiles = profileDao.getAll();
        for (Profile a : checkProfiles) {
            if (a.getName().equalsIgnoreCase(checkProfileName)) {
                return true;
            }
        }
        return false;
    }

    private void createDialogAddProfileDoubleWarning(final String doubleName, String descr, Context context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final String newProfileName = doubleName;
        final String newProfiledescr = descr;

        final TextView deleteTitle = (TextView) layout.findViewById(R.id.warning_dialog_title);
        final TextView deleteDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);

        String message = getResources().getString(R.string.dialog_profileadd_double_name_conflict_1)
                + doubleName + getResources().getString(R.string.dialog_profileadd_double_name_conflict_2);
        deleteDescr.setText(message);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile newProfile = new Profile(newProfileName, newProfiledescr);
                profileDao.insertAll(newProfile);
                joinNewProfileWithAllRecommendations();

                tempProfileNameFromDiscard = "";
                tempProfileDescrFromDiscard = "";

                String message = getResources().getString(R.string.toast_confirm_profile_creation1) + newProfileName + getResources().getString(R.string.toast_confirm_profile_creation2);
                toastMessage(message);
                recreate();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempProfileNameFromDiscard = newProfileName;
                tempProfileDescrFromDiscard = newProfiledescr;
                newProfileDiscarded = true;
                toastMessage(getResources().getString(R.string.toast_discard_add_entry));
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    private void joinNewProfileWithAllRecommendations() {
        List<Recommendation> recommendations = recommendationDao.getAll();
        int newestProfileID = profileDao.getAll().get(profileDao.getAll().size() - 1).getID();
        for (int i = 0; i < recommendations.size(); i++) {
            int recommendationID = recommendations.get(i).getID();
            recommendationProfileJoinDao.insert(new RecommendationProfileJoin(recommendationID, newestProfileID));
        }
    }


}

//profiles_help