package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Favorite {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    public String title;
    @NonNull
    public String content;
    public Integer profileID;
    public String profileName;


    //TODO: Make History table clear itself. Limit entries by 30 or so

    public Favorite(String title, @NonNull String content, Integer profileID, String profileName) {
        this.title = title;
        this.content = content;
        this.profileID = profileID;
        this.profileName = profileName;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @NonNull
    public String getContent() {
        return content;
    }
    public void setContent(@NonNull String content) {
        this.content = content;
    }

    public Integer getProfileID() {
        return profileID;
    }
    public void setProfileID(Integer profileID) {
        this.profileID = profileID;
    }

    public String getProfileName() {
        return profileName;
    }
    public void setProfileName(@NonNull String profileName) {
        this.profileName = profileName;
    }

}