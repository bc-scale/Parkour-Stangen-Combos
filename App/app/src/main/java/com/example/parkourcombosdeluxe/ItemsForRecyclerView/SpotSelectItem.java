package com.example.parkourcombosdeluxe.ItemsForRecyclerView;

import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.Spot;

public class SpotSelectItem {

    private Spot mNextSpot;
    //private int mImageResource;
    //private String mTextTitle;
    //private String mTextDescr;

    public SpotSelectItem(Spot spot) {
        mNextSpot = spot;
        //mImageResource = imageResource;
        //mTextTitle = spot.getName();
        //mTextDescr = String.valueOf(spot.getCombosDoneCount());
    }

    public void updateSpot(Spot updatedSpot) {
        mNextSpot = updatedSpot;
    }



    public Spot getSpot() {
        return mNextSpot;
    }

    public int getSpotID() {
        return mNextSpot.getID();
    }

    public String getSpotName() {
        return mNextSpot.getName();
    }

    public int getSpotProfileID() {
        return mNextSpot.getProfileID();
    }

    public int getSpotCombosDoneCount() {
        return mNextSpot.getCombosDoneCount();
    }

}
