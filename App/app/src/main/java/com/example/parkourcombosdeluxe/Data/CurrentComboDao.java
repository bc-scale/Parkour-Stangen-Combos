package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface CurrentComboDao {

    //TODO: (here and everywhere else): make this sort by ID, if necessary
    @Query("SELECT * FROM currentCombo")
    List<CurrentCombo> getAll();

    @Query("SELECT * FROM currentCombo where ID LIKE  :ID")
    CurrentCombo findByID(Integer ID);

    @Insert
    void insertAll(CurrentCombo... currentCombos);

    @Update
    void updateCurrentCombos(CurrentCombo... currentCombos);

    @Delete
    void delete(CurrentCombo currentCombo);

    @Query("DELETE FROM currentCombo")
    void deleteAll();

}