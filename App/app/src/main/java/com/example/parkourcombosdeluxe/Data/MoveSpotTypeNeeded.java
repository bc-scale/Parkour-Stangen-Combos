package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class MoveSpotTypeNeeded {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    String englishDescr;
    String germanDescr;
    String colorCoding;
    Boolean changeable;

    MoveSpotTypeNeeded(String englishDescr, String germanDescr, String colorCoding, Boolean changeable) {
        this.englishDescr = englishDescr;
        this.germanDescr = germanDescr;
        this.colorCoding = colorCoding;
        this.changeable = changeable;
    }

    @Ignore
    public MoveSpotTypeNeeded(@NonNull String englishDescr, String germanDescr, Boolean changeable) {
        this(englishDescr, germanDescr, null, changeable);
    }

    @Ignore
    public MoveSpotTypeNeeded(@NonNull String englishDescr, String germanDescr) {
        this(englishDescr, germanDescr, null, true);
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getEnglishDescr() {
        return englishDescr;
    }
    public void setEnglishDescr(String englishDescr) {
        this.englishDescr = englishDescr;
    }

    public String getColorCoding() {
        return colorCoding;
    }
    public void setColorCoding(String colorCoding) {
        this.colorCoding = colorCoding;
    }

    public String getGermanDescr() {
        return germanDescr;
    }
    public void setGermanDescr(String germanDescr) {
        this.germanDescr = germanDescr;
    }

    public Boolean getChangeable() {
        return changeable;
    }
    public void setChangeable(Boolean changeable) {
        this.changeable = changeable;
    }


}