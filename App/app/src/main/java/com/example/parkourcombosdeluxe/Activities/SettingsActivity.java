package com.example.parkourcombosdeluxe.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.ProfileDao;
import com.example.parkourcombosdeluxe.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.parkourcombosdeluxe.Data.Profile.*;

public class SettingsActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {


    //@BindView(R.id.settings_spinnerLanguageSelect)
    //Spinner spinnerLanguageSelect;

    private Locale current;

    @BindView(R.id.settings_switchNightMode)
    Switch switchNightMode;
    @BindView(R.id.settings_change_color_theme_layout)
    LinearLayout changeColorThemeLayout;
    @BindView(R.id.settings_buttonResetMoves)
    Button buttonResetMoves;
    @BindView(R.id.settings_buttonResetApp)
    Button buttonResetApp;

    Profile currentProfile;
    ProfileDao profileDao;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentProfile = getCurrentProfile();
        profileDao = AppDatabase.getInstance(this).profileDao();

        initiateButtonsEtc();

        //TODO: activate this once colors have been changed:
        activateMoveBackgroundColorCoding();
    }



    private void initiateButtonsEtc() {

        switchNightMode.setChecked(getSharedPreferences(getResources().getString(R.string.sharedpref_preferences), MODE_PRIVATE).getBoolean(getResources().getString(R.string.sharedpref_dark_theme), false));
        switchNightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleTheme(isChecked);
            }
        });


        //Change Language Spinner
        current = getResources().getConfiguration().locale;
        Spinner spinnerLanguageSelect = findViewById(R.id.settings_spinnerLanguageSelect);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.supported_languages, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLanguageSelect.setAdapter(adapter);
        setLanguageSpinnerSelection(spinnerLanguageSelect, current);
        spinnerLanguageSelect.setOnItemSelectedListener(this);


        changeColorThemeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogColorTheme(SettingsActivity.this);
            }
        });


        //Reset Moves Button
        buttonResetMoves.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle(getString(R.string.dialog_warning_title));
                builder.setMessage(getString(R.string.dialog_resetapp_text));
                builder.setPositiveButton(getString(R.string.dialog_button_confirm_warning), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        toastMessage(getString(R.string.toast_resetmoves) + "NOCH ZU IMPLEMENTIEREN");
                        //TODO: wirklich resetten, danach String hierüber rausnehmen
                    }
                });
                builder.setNegativeButton(getString(R.string.dialog_button_cancel_warning), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        return;
                    }
                });
                builder.show();

            }

        });


        //Reset App Button
        buttonResetApp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle(getString(R.string.dialog_warning_title));
                builder.setMessage(getString(R.string.dialog_resetapp_text));
                builder.setPositiveButton(getString(R.string.dialog_button_confirm_warning), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        toastMessage(getString(R.string.toast_resetApp) + "NOCH ZU IMPLEMENTIEREN");
                        //TODO: wirklich resetten, danach String hierüber rausnehmen
                    }
                });
                builder.setNegativeButton(getString(R.string.dialog_button_cancel_warning), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        return;
                    }
                });
                builder.show();

            }

        });
    }


    private void toggleTheme(boolean isDarkTheme) {
        getSharedPreferences(getResources().getString(R.string.sharedpref_preferences), MODE_PRIVATE)
                .edit()
                .putBoolean(getResources().getString(R.string.sharedpref_dark_theme), isDarkTheme)
                .apply();
        makeMainRecreate();
        recreate();
        /*Intent intent = getIntent();
        finish();
        startActivity(intent);*/
    }

    private void activateMoveBackgroundColorCoding() {
        sharedPref.edit().putBoolean(getResources().getString(R.string.sharedpref_move_card_color_coding), true).apply();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        String languageToLoad = "";
        boolean changed = false;
        switch (pos) {
            case 0:
                languageToLoad = "en";
                if (!languageToLoad.equals(current.getLanguage())) {
                    changed = true;
                }
                break;
            case 1:
                languageToLoad = "de";
                if (!languageToLoad.equals(current.getLanguage())) {
                    changed = true;
                }
                break;
            default:
                break;
        }
        if (changed) {
            changeLanguage(languageToLoad);
            Toast.makeText(parent.getContext(), getResources().getString(R.string.toast_language_changed),
                    Toast.LENGTH_SHORT).show();
            this.recreate();
            //this.getParent().recreate();
            //Bleibt aber nicht dauerhaft........
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        return;
    }

    private void setLanguageSpinnerSelection(Spinner spinner, Locale locale) {
        String checkLanguage = locale.getLanguage();
        int spinnerPos = 0;
        switch (checkLanguage) {
            case "en":
                spinnerPos = 0;
                break;
            case "de":
                spinnerPos = 1;
                break;
        }
        spinner.setSelection(spinnerPos);
    }



    private void createDialogColorTheme(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_color_theme_select, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        LinearLayout textStandard = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_standard);
        textStandard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_STANDARD);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textLion = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_lion);
        textLion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_LION);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textEFDark = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_ef_dark);
        textEFDark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_EF_DARK);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textEFLight = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_ef_light);
        textEFLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_EF_LIGHT);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textFDroidDark = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_fdroid_dark);
        textFDroidDark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_FDROID_DARK);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textAndroidLight = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_android);
        textAndroidLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_ANDROID);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textUbuntu = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_ubuntu_tux);
        textUbuntu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_UBUNTU_TUX);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textK9 = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_k9_red);
        textK9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_K9_RED);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textAuxTraditional = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_auxcastle_traditional);
        textAuxTraditional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_AUXCASTLE_TRADITIONAL);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textSimpleOrange = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_simple_orange);
        textSimpleOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_SIMPLE_ORANGE);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textCMYLight = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_cmy_light);
        textCMYLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_CMY_LIGHT);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textRGBLight = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_rgb_light);
        textRGBLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_RGB_LIGHT);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textMonochrome = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_monochrome);
        textMonochrome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_MONOCHROME);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textGoogle = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_google);
        textGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_GOOGLE);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        LinearLayout textGooglePlay = (LinearLayout) layout.findViewById(R.id.settings_color_theme_select_google_play);
        textGooglePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentProfile.setCurrentColorTheme(COLOR_THEME_GOOGLE_PLAY);
                profileDao.updateProfiles(currentProfile);

                makeMainRecreate();
                alertDialog.dismiss();
                SettingsActivity.this.recreate();
            }
        });

        alertDialog.show();
    }




    private void changeLanguage(String language) {
        Locale changeLocale;
        changeLocale = new Locale(language);
        Locale.setDefault(changeLocale);
        Configuration config = new Configuration();
        config.locale = changeLocale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getResources().getString(R.string.sharedpref_current_selected_language), language);
        editor.apply();

        makeMainRecreate();
        recreate();
    }


}