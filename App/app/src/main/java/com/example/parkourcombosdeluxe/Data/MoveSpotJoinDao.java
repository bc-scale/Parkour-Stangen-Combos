package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MoveSpotJoinDao {

    @Insert
    void insert(MoveSpotJoin moveSpotJoin);

    @Query("SELECT * FROM move INNER JOIN move_spot_join " +
            "ON move.ID=move_spot_join.moveID " +
            " WHERE move_spot_join.spotID=:spotIDIn")
    List<Move> getMovesForSpot(final Integer spotIDIn);

    @Query("SELECT ID, englishName, germanName, favorite FROM move INNER JOIN move_spot_join " +
            "ON move.ID=move_spot_join.moveID " +
            " WHERE move_spot_join.spotID=:spotIDIn")
    List<MoveMinimal> getMovesForSpotMinimal(final Integer spotIDIn);

    @Query("SELECT ID FROM move INNER JOIN move_spot_join " +
            "ON move.ID=move_spot_join.moveID " +
            " WHERE move_spot_join.spotID=:spotIDIn")
    List<Integer> getMoveIDsForSpot(final Integer spotIDIn);

    @Query("SELECT * FROM spot INNER JOIN move_spot_join " +
            "ON spot.ID=move_spot_join.spotID " +
            " WHERE move_spot_join.moveID=:moveIDIn")
    List<Spot> getSpotsForMove(final Integer moveIDIn);

/*    //INSERT isn't supported yet, so workaround with getMovesForSpot and insert in MainActivity
    @Query("INSERT INTO move_spot_join " +
            "SELECT moveID, :spotReplicaID " +
            "FROM move_spot_join " +
            " WHERE move_spot_join.spotID=:spotSourceID")
    void copyMovesFromSpotToEmptySpot(final int spotSourceID, final int spotReplicaID);*/

    @Delete
    void delete(MoveSpotJoin moveSpotJoin);

    @Query("DELETE FROM move_spot_join")
    void deleteAll();
}