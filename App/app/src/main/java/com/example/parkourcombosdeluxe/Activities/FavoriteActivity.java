package com.example.parkourcombosdeluxe.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.Favorite;
import com.example.parkourcombosdeluxe.Data.FavoriteDao;
import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.ProfileDao;
import com.example.parkourcombosdeluxe.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteActivity extends BaseActivity {

    @BindView(R.id.layout_FavoriteScroll)
    LinearLayout layout_FavoriteScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        favoriteDao = getDBs.favoriteDao();
        profileDao = getDBs.profileDao();

        generateFavorite();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorites_favorite_add:
                createDialogAddFavorite(FavoriteActivity.this);
                break;
            case R.id.action_favorites_help:
                createDialogHelpBasic(FavoriteActivity.this, getResources().getString(R.string.favorites_dialog_help_title), getResources().getString(R.string.favorites_dialog_help_text));
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    private void generateFavorite() {

        List<Favorite> favorites;
        if (sharedPref.getBoolean(getString(R.string.sharedpref_favorites_multiprofile), false)) {
            favorites = favoriteDao.getAll();
        } else {
            favorites = favoriteDao.getAllByProfileID(currentProfile.getID());
        }

        layout_FavoriteScroll.removeAllViews();

        if (favorites.size() == 0) {
            generateMessageEmptyFavorite();
        } else {
            //go through backwards, to get the newest combo on top
            for (int i = favorites.size() - 1; i >= 0; i--) {
                //ALTER give the textview margin within a layout
                LinearLayout.LayoutParams paramsContent = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramsContent.leftMargin = 30;
                LinearLayout.LayoutParams paramsEdit = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                //TODO: get exact size needed
                paramsEdit.height = (int) (getResources().getDimensionPixelSize(R.dimen.scrollview_title) * 1.5);

                Favorite nextFavorite = favorites.get(i);

                LinearLayout favEntryWhole = new LinearLayout(FavoriteActivity.this);
                favEntryWhole.setOrientation(LinearLayout.VERTICAL);

                LinearLayout upperHalf = new LinearLayout(FavoriteActivity.this);

                LinearLayout lowerHalf = new LinearLayout(FavoriteActivity.this);
                //lowerHalf.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));

                LinearLayout withFav = new LinearLayout(FavoriteActivity.this);
                withFav.setOrientation(LinearLayout.VERTICAL);

                TextView comboNameAndProfile = new TextView(FavoriteActivity.this);
                if (nextFavorite.getTitle().length() < 1) {
                    comboNameAndProfile.setText(getResources().getString(R.string.plain_combo) + " " + nextFavorite.getID().toString() + "   (" + profileDao.findByID(nextFavorite.getProfileID()).getName() + ")   ");
                } else {
                    comboNameAndProfile.setText(nextFavorite.getTitle() + "   (" + profileDao.findByID(nextFavorite.getProfileID()).getName() + ")   ");
                }
                comboNameAndProfile.setTextColor(getResources().getColor(R.color.colorTextLight));
                comboNameAndProfile.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_title));
                comboNameAndProfile.setTypeface(Typeface.DEFAULT_BOLD);

                //ALTER makes the rest of the views that share space in a Layout with this view go to the edge of the Layout
                comboNameAndProfile.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1.0f));

                ImageButton editComboName = new ImageButton(FavoriteActivity.this);
                editComboName.setImageDrawable(getResources().getDrawable(R.drawable.iconn_pen_edit_diagonal));
                editComboName.setColorFilter(getResources().getColor(R.color.colorButtonLight));
                editComboName.setId(nextFavorite.getID());
                editComboName.setScaleType(ImageView.ScaleType.CENTER_CROP);
                editComboName.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        createDialogEditFavorite(view, FavoriteActivity.this);
                    }

                });


                ImageView favHeart = new ImageView(FavoriteActivity.this);
                favHeart.setImageDrawable(getResources().getDrawable(R.drawable.icon_heart_fill));
                favHeart.setColorFilter(getResources().getColor(R.color.colorRedFavorite));

                ImageButton deleteFavTrashBin = new ImageButton(FavoriteActivity.this);
                deleteFavTrashBin.setImageDrawable(getResources().getDrawable(R.drawable.ic_trash_bin));
                deleteFavTrashBin.setColorFilter(getResources().getColor(R.color.colorButtonLight));
                deleteFavTrashBin.setId(nextFavorite.getID());
                deleteFavTrashBin.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        createDialogDeleteFavorite(view, FavoriteActivity.this);
                    }

                });

                TextView content = new TextView(FavoriteActivity.this);
                content.setText(nextFavorite.getContent());
                content.setTextColor(getResources().getColor(R.color.colorTextLight));
                content.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_descr));

                //add views
                upperHalf.addView(comboNameAndProfile);
                upperHalf.addView(editComboName, paramsEdit);

                withFav.addView(favHeart);
                withFav.addView(deleteFavTrashBin);

                lowerHalf.addView(withFav);
                lowerHalf.addView(content, paramsContent);

                favEntryWhole.addView(upperHalf);
                favEntryWhole.addView(lowerHalf);

                layout_FavoriteScroll.addView(favEntryWhole);
            }
        }
    }

    private void generateMessageEmptyFavorite() {
        //TODO clean up
        TextView child = new TextView(FavoriteActivity.this);
        child.setWidth(layout_FavoriteScroll.getWidth());
        child.setText(getResources().getString(R.string.favorite_empty_message));
        child.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        child.setTextColor(getResources().getColor(R.color.colorTextLight));
        child.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.main_text_combo_normal));
        layout_FavoriteScroll.addView(child);
    }





    private void toggleMultiProfileView() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.sharedpref_favorites_multiprofile), !(sharedPref.getBoolean(getString(R.string.sharedpref_favorites_multiprofile), true)));
        editor.commit();
        recreate();
    }

    private void createDialogDeleteFavorite(View view, Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final int tempID = view.getId();
        final Favorite tempFavorite = favoriteDao.findByID(tempID);

        final TextView deleteTitle = (TextView) layout.findViewById(R.id.warning_dialog_title);
        final TextView deleteDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);

        deleteDescr.setText(getString(R.string.dialog_delete_entry) + " " + tempFavorite.getTitle());

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteDao.delete(tempFavorite);
                toastMessage(getString(R.string.toast_confirm_delete_entry));
                alertDialog.dismiss();
                recreate();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    private void createDialogEditFavorite(View view, Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_edit_favorite, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final int tempID = view.getId();
        final Favorite tempFavorite = favoriteDao.findByID(tempID);

        final TextView editTitle = (TextView) layout.findViewById(R.id.edit_favorite_dialog_title);
        final EditText editName = (EditText) layout.findViewById(R.id.edit_favorite_dialog_name);
        final EditText editDescr = (EditText) layout.findViewById(R.id.edit_favorite_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.edit_favorite_dialog_button_save);
        Button cancelButton = (Button) layout.findViewById(R.id.edit_favorite_dialog_button_cancel);

        editTitle.setText(getString(R.string.dialog_edit_combo_name_title));
        editName.setText(tempFavorite.getTitle());
        editDescr.setText(tempFavorite.getContent());

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteDao.delete(tempFavorite);

                tempFavorite.setTitle(editName.getText().toString());
                tempFavorite.setContent(editDescr.getText().toString());

                favoriteDao.insertAll(tempFavorite);

                toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                alertDialog.dismiss();
                recreate();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    //TODO ask to copy current Spot (from profile currently used) to new Profile after creating new profile
    private void createDialogAddFavorite(Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_create_twoliner, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final TextView addTitle = (TextView) layout.findViewById(R.id.add_twoliner_dialog_title);
        final EditText newName = (EditText) layout.findViewById(R.id.add_twoliner_dialog_name);
        final EditText newDescr = (EditText) layout.findViewById(R.id.add_twoliner_dialog_descr);
        Button saveButton = (Button) layout.findViewById(R.id.add_twoliner_dialog_button_save);
        Button cancelButton = (Button) layout.findViewById(R.id.add_twoliner_dialog_button_cancel);

        addTitle.setText(getResources().getString(R.string.dialog_create_favorite_title));

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String newFavoriteName = newName.getText().toString();
                final String newFavoriteDescr = newDescr.getText().toString();

                if (newFavoriteName.isEmpty()) {
                    toastMessage(getResources().getString(R.string.toast_favorite_name_empty));
                } else {
                    Favorite newFavorite = new Favorite(newFavoriteName, newFavoriteDescr, currentProfile.getID(), currentProfile.getName());
                    favoriteDao.insertAll(newFavorite);

                    String message = getResources().getString(R.string.toast_confirm_favorite_creation1) + newFavoriteName + getResources().getString(R.string.toast_confirm_favorite_creation2);
                    toastMessage(message);
                    alertDialog.dismiss();
                    FavoriteActivity.this.recreate();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


}