package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity
public class Move {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    Integer styleOrDifficulty;
    Integer spotTypeNeeded;
    Integer movementType;
    String englishName;
    String germanName;
    Boolean favorite;
    Boolean archived;
    Boolean changed;
    String iconColor;


    public Move(Integer styleOrDifficulty, Integer spotTypeNeeded, Integer movementType, String englishName, String germanName, Boolean favorite, Boolean archived, Boolean changed, String iconColor) {
        this.styleOrDifficulty = styleOrDifficulty;
        this.spotTypeNeeded = spotTypeNeeded;
        this.movementType = movementType;
        this.englishName = englishName;
        this.germanName = germanName;
        this.favorite = favorite;
        this.archived = archived;
        this.changed = changed;
        this.iconColor = iconColor;
    }

    @Ignore
    public Move(String englishName, String germanName) {
        this(99, 99, 1, englishName, germanName, false, false, true, null);
    }

    @Ignore
    public Move(Integer styleOrDifficulty, Integer spotTypeNeeded, Integer movementType, String englishName, String germanName, Boolean changed) {
        this(styleOrDifficulty, spotTypeNeeded, movementType, englishName, germanName, false, false, changed, null);
    }

    @Ignore
    public Move(Integer styleOrDifficulty, Integer spotTypeNeeded, String englishName, String germanName, String iconColor) {
        this(styleOrDifficulty, spotTypeNeeded, 1, englishName, germanName, false, false, false, iconColor);
    }

    @Ignore
    public Move(Integer styleOrDifficulty, Integer spotTypeNeeded, String englishName, String germanName) {
        this(styleOrDifficulty, spotTypeNeeded, 1, englishName, germanName, false, false, false, null);
    }



    //99 shall be undefined
    @Ignore
    public Move(@NonNull String englishName) {
        this(99, 99, 1, englishName, "", false, false, true, null);
    }

    public Integer getID() { return ID; }
    public void setID(Integer ID) { this.ID = ID; }

    public Integer getStyleOrDifficulty() {
        return styleOrDifficulty;
    }
    public void setStyleOrDifficulty(Integer styleOrDifficulty) {
        this.styleOrDifficulty = styleOrDifficulty;
    }

    public Integer getSpotTypeNeeded() {
        return spotTypeNeeded;
    }
    public void setSpotTypeNeeded(Integer spotTypeNeeded) {
        this.spotTypeNeeded = spotTypeNeeded;
    }

    public Integer getMovementType() {
        return movementType;
    }
    public void setMovementType(Integer movementType) {
        this.movementType = movementType;
    }

    public String getEnglishName() {
        return englishName;
    }
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getGermanName() {
        return germanName;
    }
    public void setGermanName(String germanName) {
        this.germanName = germanName;
    }

    public Boolean getFavorite() {
        return favorite;
    }
    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Boolean getArchived() {
        return archived;
    }
    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Boolean getChanged() {
        return changed;
    }
    public void setChanged(Boolean changed) {
        this.changed = changed;
    }

    public String getIconColor() {
        return iconColor;
    }
    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }



}