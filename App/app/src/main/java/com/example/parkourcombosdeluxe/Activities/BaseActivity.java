package com.example.parkourcombosdeluxe.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.CurrentComboDao;
import com.example.parkourcombosdeluxe.Data.FavoriteDao;
import com.example.parkourcombosdeluxe.Data.HistoryDao;
import com.example.parkourcombosdeluxe.Data.Move;
import com.example.parkourcombosdeluxe.Data.MoveDao;
import com.example.parkourcombosdeluxe.Data.MoveMinimal;
import com.example.parkourcombosdeluxe.Data.MoveSpotJoin;
import com.example.parkourcombosdeluxe.Data.MoveSpotJoinDao;
import com.example.parkourcombosdeluxe.Data.MoveSpotTypeNeeded;
import com.example.parkourcombosdeluxe.Data.MoveSpotTypeNeededDao;
import com.example.parkourcombosdeluxe.Data.MoveStyleOrDifficulty;
import com.example.parkourcombosdeluxe.Data.MoveStyleOrDifficultyDao;
import com.example.parkourcombosdeluxe.Data.MovementTypeDao;
import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.ProfileDao;
import com.example.parkourcombosdeluxe.Data.Recommendation;
import com.example.parkourcombosdeluxe.Data.RecommendationDao;
import com.example.parkourcombosdeluxe.Data.RecommendationProfileJoinDao;
import com.example.parkourcombosdeluxe.Data.Spot;
import com.example.parkourcombosdeluxe.Data.SpotDao;
import com.example.parkourcombosdeluxe.Data.Tipp;
import com.example.parkourcombosdeluxe.Data.TippDao;
import com.example.parkourcombosdeluxe.R;

import java.util.List;
import java.util.Locale;

import static com.example.parkourcombosdeluxe.Data.Profile.*;


public abstract class BaseActivity extends AppCompatActivity {

    protected SharedPreferences sharedPref;

    protected Profile currentProfile;
    protected Spot currentSpot;

    protected AppDatabase getDBs;

    protected CurrentComboDao currentComboDao;
    protected FavoriteDao favoriteDao;
    protected HistoryDao historyDao;
    protected MoveDao moveDao;
    protected MovementTypeDao movementTypeDao;
    protected MoveSpotJoinDao moveSpotJoinDao;
    protected MoveSpotTypeNeededDao moveSpotTypeNeededDao;
    protected MoveStyleOrDifficultyDao moveStyleOrDifficultyDao;
    protected ProfileDao profileDao;
    protected RecommendationDao recommendationDao;
    protected RecommendationProfileJoinDao recommendationProfileJoinDao;
    protected SpotDao spotDao;
    protected TippDao tippDao;

    protected boolean noSpotForCurrentProfile = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        checkFirstRun();

        getDBs = AppDatabase.getInstance(this);
        currentProfile = getCurrentProfile();

        setTheme();
        super.onCreate(savedInstanceState);
    }

    private void setTheme() {
        boolean isFirstRun = getSharedPreferences(getResources().getString(R.string.sharedpref_preferences), MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (getSharedPreferences(getResources().getString(R.string.sharedpref_preferences), MODE_PRIVATE).getBoolean(getResources().getString(R.string.sharedpref_dark_theme), false)) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        } else if (isFirstRun) {
            setTheme(R.style.AppTheme_Light_NoActionBar);
        } else {
            switch (getCurrentProfile().getCurrentColorTheme()) {
                case COLOR_THEME_STANDARD:
                    setTheme(R.style.AppTheme_Light_NoActionBar);
                    break;
                case COLOR_THEME_LION:
                    setTheme(R.style.AppTheme_Lion_NoActionBar);
                    break;
                case COLOR_THEME_EF_DARK:
                    setTheme(R.style.AppTheme_PkoneDark_NoActionBar);
                    break;
                case COLOR_THEME_EF_LIGHT:
                    setTheme(R.style.AppTheme_PkoneGrey_NoActionBar);
                    break;
                case COLOR_THEME_FDROID_DARK:
                    setTheme(R.style.AppTheme_Fdroid_NoActionBar);
                    break;
                case COLOR_THEME_ANDROID:
                    setTheme(R.style.AppTheme_Android_NoActionBar);
                    break;
                case COLOR_THEME_UBUNTU_TUX:
                    setTheme(R.style.AppTheme_Ubuntu_NoActionBar);
                    break;
                case COLOR_THEME_K9_RED:
                    setTheme(R.style.AppTheme_K9_NoActionBar);
                    break;
                case COLOR_THEME_AUXCASTLE_TRADITIONAL:
                    setTheme(R.style.AppTheme_AuxCastleTraditional_NoActionBar);
                    break;
                case COLOR_THEME_SIMPLE_ORANGE:
                    setTheme(R.style.AppTheme_SimpleOrange_NoActionBar);
                    break;
                case COLOR_THEME_CMY_LIGHT:
                    setTheme(R.style.AppTheme_PrintCMY_NoActionBar);
                    break;
                case COLOR_THEME_RGB_LIGHT:
                    setTheme(R.style.AppTheme_RGB_NoActionBar);
                    break;
                case COLOR_THEME_MONOCHROME:
                    setTheme(R.style.AppTheme_Monochrome_NoActionBar);
                    break;
                case COLOR_THEME_GOOGLE:
                    setTheme(R.style.AppTheme_Google_NoActionBar);
                    break;
                case COLOR_THEME_GOOGLE_PLAY:
                    setTheme(R.style.AppTheme_GooglePlay_NoActionBar);
                    break;
                default:
                    setTheme(R.style.AppTheme_Light_NoActionBar);
                    break;
            }
        }
    }



    protected void checkFirstRun() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {
            AppDatabase.getInstance(this).resetDatabase();
            //TODO: Place your dialog code here to display the dialog

            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRun", false)
                    .apply();

            welcomeFirstRunSettings();
        }
    }

    protected void welcomeFirstRunSettings() {
        //Set Profile as the preinstalled default profile
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.sharedpref_current_profileid), 1);
        editor.putString(getResources().getString(R.string.sharedpref_current_selected_language), Locale.getDefault().getLanguage());
        editor.putBoolean(getResources().getString(R.string.sharedpref_favorites_multiprofile), false);
        editor.commit();

        //Check first run
        toastMessageShort(getString(R.string.toast_first_run));
    }

    protected Profile getCurrentProfile() {
        Profile returnProfile = getDBs.profileDao().findByID(PreferenceManager.getDefaultSharedPreferences(this).getInt(getResources().getString(R.string.sharedpref_current_profileid), 1));

        if (returnProfile == null)
        {
            returnProfile = getDBs.profileDao().getAll().get(0);
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putInt(getResources().getString(R.string.sharedpref_current_profileid), returnProfile.getID());
            editor.commit();
        }
        return returnProfile;
    }


    protected void setCurrentSpotIfThereAreAny() {
        if (spotDao.getAllByProfileID(currentProfile.getID()).size() < 1) {
            //there is no spot belonging to this profile
            currentProfile.setCurrentSpotId(null);
            noSpotForCurrentProfile = true;
        } else {
            //check if the spot saved in the profile even belongs to this profile
            if (currentProfile.getCurrentSpotId() == null) {
                //sets first available spot for this profile as current spot
                currentProfile.setCurrentSpotId(spotDao.getAllByProfileID(currentProfile.getID()).get(0).getID());
                currentSpot = spotDao.findByID(currentProfile.getCurrentSpotId());
                profileDao.updateProfiles(currentProfile);
            } else {
                if (currentProfile.getID().equals(spotDao.findByID(currentProfile.getCurrentSpotId()).getProfileID())) {
                    //profiles SpotID is correct, i.e. the spot belongs to this profile
                    currentSpot = spotDao.findByID(currentProfile.getCurrentSpotId());
                } else {
                    //profile's spotID points to a Spot that doesn't belong to the profile
                    currentSpot = spotDao.getAllByProfileID(currentProfile.getID()).get(0);
                    currentProfile.setCurrentSpotId(currentSpot.getID());
                    profileDao.updateProfiles(currentProfile);
                }
            }
            noSpotForCurrentProfile = false;
        }
    }

    protected void copyMovesFromSpotToEmptySpot(int spotSourceID, int spotEmptyReplicaID) {
        List<Move> movesFromSource = moveSpotJoinDao.getMovesForSpot(spotSourceID);
        for (Move move: movesFromSource) {
            moveSpotJoinDao.insert(new MoveSpotJoin(move.getID(), spotEmptyReplicaID));
        }
    }

    protected void makeMainRecreate() {
        SharedPreferences sharedPrefRecreate = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPrefRecreate.edit();
        editor.putBoolean(getResources().getString(R.string.sharedpref_main_just_created), false);
        editor.apply();
    }

    protected void toastMessage (String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    protected void toastMessageShort(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    protected void changeLanguage() {
        Locale changeLocale;
        changeLocale = new Locale(sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en"));
        Locale.setDefault(changeLocale);
        Configuration config = new Configuration();
        config.locale = changeLocale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    protected void createDialogHelpBasic(Context context, String titleText, String descrText) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_help_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        TextView helpTitle = (TextView) layout.findViewById(R.id.help_dialog_title);
        TextView helpDescr = (TextView) layout.findViewById(R.id.help_dialog_descr);
        Button helpButtonOk = (Button) layout.findViewById(R.id.help_dialog_button_ok);

        helpTitle.setText(titleText);
        helpTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        helpDescr.setText(descrText);
        helpButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);

        alertDialog.show();
    }

    protected String getLocalisedMoveName(Move move) {
        switch (sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en")) {
            case "en":
                return move.getEnglishName();
            case "de":
                return move.getGermanName();
            default:
                return move.getEnglishName();
        }
    }

    protected String getLocalisedMoveMinimalName(MoveMinimal move) {
        switch (sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en")) {
            case "en":
                return move.getEnglishName();
            case "de":
                return move.getGermanName();
            default:
                return move.getEnglishName();
        }
    }

    protected String getLocalisedRecommendationDescr(Recommendation recommendation) {
        switch (sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en")) {
            case "en":
                return recommendation.getEnglishDescr();
            case "de":
                return recommendation.getGermanDescr();
            default:
                return recommendation.getEnglishDescr();
        }
    }

    protected String getLocalisedTippDescr(Tipp tipp) {
        switch (sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en")) {
            case "en":
                return tipp.getEnglishDescr();
            case "de":
                return tipp.getGermanDescr();
            default:
                return tipp.getEnglishDescr();
        }
    }

    protected String getLocalisedMoveStyleOrDifficultyDescr(int diffID, String language) {
        try {
            switch (language) {
                case "en":
                    return moveStyleOrDifficultyDao.findByID(diffID).getEnglishDescr();
                case "de":
                    return moveStyleOrDifficultyDao.findByID(diffID).getGermanDescr();
                default:
                    return moveStyleOrDifficultyDao.findByID(diffID).getEnglishDescr();
            }
        } catch (NullPointerException e) {
            return "error :(";
        }
    }

    protected String getLocalisedMoveSpotTypeNeededDescr(int spotTypeID, String language) {
        try {
            switch (language) {
                case "en":
                    return moveSpotTypeNeededDao.findByID(spotTypeID).getEnglishDescr();
                case "de":
                    return moveSpotTypeNeededDao.findByID(spotTypeID).getGermanDescr();
                default:
                    return moveSpotTypeNeededDao.findByID(spotTypeID).getEnglishDescr();
            }
        } catch (NullPointerException e) {
            return "error :(";
        }
    }

    protected String getLocalisedMovementTypeDescr(int movementTypeID, String language) {
        try {
            switch (language) {
                case "en":
                    return movementTypeDao.findByID(movementTypeID).getEnglishDescr();
                case "de":
                    return movementTypeDao.findByID(movementTypeID).getGermanDescr();
                default:
                    return movementTypeDao.findByID(movementTypeID).getEnglishDescr();
            }
        } catch (NullPointerException e) {
            return "error :(";
        }
    }
}
