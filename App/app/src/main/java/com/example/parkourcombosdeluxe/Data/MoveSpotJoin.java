package com.example.parkourcombosdeluxe.Data;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "move_spot_join",
        primaryKeys = {"moveID", "spotID"},
        foreignKeys = {
                @ForeignKey(entity = Move.class,
                        parentColumns = "ID",
                        childColumns = "moveID",
                        onDelete = CASCADE),
                @ForeignKey(entity = Spot.class,
                        parentColumns = "ID",
                        childColumns = "spotID",
                        onDelete = CASCADE)
        })
public class MoveSpotJoin {
    @NonNull
    public final Integer moveID;
    @NonNull
    public final Integer spotID;

    public MoveSpotJoin(@NonNull Integer moveID, @NonNull Integer spotID) {
        this.moveID = moveID;
        this.spotID = spotID;
    }
}