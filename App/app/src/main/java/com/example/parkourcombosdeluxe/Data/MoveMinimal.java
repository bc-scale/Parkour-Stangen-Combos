package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity
public class MoveMinimal {
    public Integer ID;
    String englishName;
    String germanName;
    Boolean favorite;


    public MoveMinimal(Integer ID, String englishName, String germanName, Boolean favorite) {
        this.ID = ID;
        this.englishName = englishName;
        this.germanName = germanName;
        this.favorite = favorite;
    }



    public Integer getID() { return ID; }
    public void setID(Integer ID) { this.ID = ID; }

    public String getEnglishName() {
        return englishName;
    }
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getGermanName() {
        return germanName;
    }
    public void setGermanName(String germanName) {
        this.germanName = germanName;
    }

    public Boolean getFavorite() {
        return favorite;
    }
    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

}