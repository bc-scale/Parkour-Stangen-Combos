package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Recommendation {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    String englishDescr;
    String germanDescr;
    Boolean secret;
    Integer standardWeight;

    Recommendation(String englishDescr, String germanDescr, Boolean secret, Integer standardWeight) {
        this.englishDescr = englishDescr;
        this.germanDescr = germanDescr;
        this.secret = secret;
        this.standardWeight = standardWeight;
    }

    //Calculation is done with Weight 0 to 200, bus user can only set it to a max of 100
    @Ignore
    public Recommendation(@NonNull String englishDescr, String germanDescr) {
        this(englishDescr, germanDescr, false, 50);
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getEnglishDescr() {
        return englishDescr;
    }
    public void setEnglishDescr(String englishDescr) {
        this.englishDescr = englishDescr;
    }

    public String getGermanDescr() {
        return germanDescr;
    }
    public void setGermanDescr(String germanDescr) {
        this.germanDescr = germanDescr;
    }

    public Boolean getSecret() {
        return secret;
    }
    public void setSecret(Boolean secret) {
        this.secret = secret;
    }

    public Integer getStandardWeight() {
        return standardWeight;
    }
    public void setStandardWeight(Integer standardWeight) {
        this.standardWeight = standardWeight;
    }


}