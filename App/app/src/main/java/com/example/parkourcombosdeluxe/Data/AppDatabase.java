package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = {Profile.class, Spot.class, Move.class, MoveStyleOrDifficulty.class,
        MoveSpotTypeNeeded.class, MovementType.class, Recommendation.class, MoveSpotJoin.class,
        RecommendationProfileJoin.class, History.class, Favorite.class, CurrentCombo.class, Tipp.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {


    private static final String DB_NAME = "AppDatabase.db";
    private static volatile AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                DB_NAME).allowMainThreadQueries().build();
    }

    public void resetDatabase() {
        resetProfiles();
        resetSpots();
        resetRecommendations();
        resetTipps();

        resetMoves();
        resetMoveStyleOrDifficulty();
        resetMoveSpotTypeNeeded();
        resetMovementTypes();

        resetCurrentCombos();

        resetMoveSpotJoins();
        resetRecommendationProfileJoins();
    }

    private void resetMoveSpotJoins() {
        MoveSpotJoinDao moveSpotJoinDao = instance.moveSpotJoinDao();
        moveSpotJoinDao.deleteAll();
        moveSpotJoinDao.insert(new MoveSpotJoin(1, 1));
        moveSpotJoinDao.insert(new MoveSpotJoin(2, 1));
        moveSpotJoinDao.insert(new MoveSpotJoin(3, 1));
        moveSpotJoinDao.insert(new MoveSpotJoin(4, 1));
        moveSpotJoinDao.insert(new MoveSpotJoin(1, 2));
        moveSpotJoinDao.insert(new MoveSpotJoin(4, 2));
        moveSpotJoinDao.insert(new MoveSpotJoin(7, 2));


        moveSpotJoinDao.insert(new MoveSpotJoin(2, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(5, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(6, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(7, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(15, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(17, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(18, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(19, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(20, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(21, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(22, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(23, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(24, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(34, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(37, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(93, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(94, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(127, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(128, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(129, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(130, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(131, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(132, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(133, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(134, 5));
        moveSpotJoinDao.insert(new MoveSpotJoin(135, 5));


        moveSpotJoinDao.insert(new MoveSpotJoin(14, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(25, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(30, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(31, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(35, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(38, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(41, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(42, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(51, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(56, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(57, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(60, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(71, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(80, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(84, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(85, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(86, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(87, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(89, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(96, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(98, 6));

        moveSpotJoinDao.insert(new MoveSpotJoin(2, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(5, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(6, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(7, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(15, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(17, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(18, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(19, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(20, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(21, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(22, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(23, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(24, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(34, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(37, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(93, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(94, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(127, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(128, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(129, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(130, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(131, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(132, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(133, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(134, 6));
        moveSpotJoinDao.insert(new MoveSpotJoin(135, 6));



        moveSpotJoinDao.insert(new MoveSpotJoin(28, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(32, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(39, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(40, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(43, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(44, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(45, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(46, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(47, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(52, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(53, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(54, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(55, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(56, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(57, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(58, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(59, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(60, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(62, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(63, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(64, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(65, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(67, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(68, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(69, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(70, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(71, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(72, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(73, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(74, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(75, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(76, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(77, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(81, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(92, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(97, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(99, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(100, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(101, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(104, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(105, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(106, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(107, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(108, 7));

        moveSpotJoinDao.insert(new MoveSpotJoin(14, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(25, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(30, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(31, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(35, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(38, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(41, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(42, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(51, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(80, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(84, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(85, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(86, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(87, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(89, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(96, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(98, 7));

        moveSpotJoinDao.insert(new MoveSpotJoin(2, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(5, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(6, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(7, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(15, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(17, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(18, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(19, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(20, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(21, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(22, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(23, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(24, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(34, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(37, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(93, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(94, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(127, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(128, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(129, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(130, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(131, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(132, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(133, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(134, 7));
        moveSpotJoinDao.insert(new MoveSpotJoin(135, 7));

    }

    private void resetRecommendationProfileJoins() {
        RecommendationProfileJoinDao recommendationProfileJoinDao = instance.recommendationProfileJoinDao();
        recommendationProfileJoinDao.deleteAll();
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(1, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(2, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(3, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(4, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(5, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(6, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(7, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(8, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(9, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(10, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(11, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(12, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(13, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(14, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(15, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(16, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(17, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(18, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(19, 1));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(1, 2));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(2, 2));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(3, 2));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(4, 2));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(5, 2));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(11, 3));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(12, 3));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(13, 3));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(14, 3));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(15, 3));


        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(1, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(2, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(3, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(4, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(5, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(6, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(7, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(8, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(9, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(10, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(11, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(12, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(13, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(14, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(15, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(16, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(17, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(18, 5));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(19, 5));


        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(1, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(2, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(3, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(4, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(5, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(6, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(7, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(8, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(9, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(10, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(11, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(12, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(13, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(14, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(15, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(16, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(17, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(18, 6));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(19, 6));


        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(1, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(2, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(3, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(4, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(5, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(6, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(7, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(8, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(9, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(10, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(11, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(12, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(13, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(14, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(15, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(16, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(17, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(18, 7));
        recommendationProfileJoinDao.insert(new RecommendationProfileJoin(19, 7));
    }



    private void resetProfiles() {
        ProfileDao profileDao = instance.profileDao();
        profileDao.deleteAll();
        profileDao.insertAll(new Profile("Main", "My main profile for training new moves.", 2));
        profileDao.insertAll(new Profile("Combos for when I'm feeling jazzy", "Whenever I encounter a double bar...", 3));
        profileDao.insertAll(new Profile("Test Profile", "For letting friends play around :-)", 4));
        profileDao.insertAll(new Profile("For my students"));

        profileDao.insertAll(new Profile("Einfach", "Für die leichten Basics", 5));
        profileDao.insertAll(new Profile("Normal", "Für die leichten Basics", 6));
        profileDao.insertAll(new Profile("Schwer", "Für die leichten Basics", 7));



    }

    private void resetSpots() {
        SpotDao spotDao = instance.spotDao();
        spotDao.deleteAll();
        spotDao.insertAll(new Spot(1, "P0 Spot 1", 0));
        spotDao.insertAll(new Spot(1,"p0 Spot2", 0));
        spotDao.insertAll(new Spot(2,"p1 Schwabencenter", 0));
        spotDao.insertAll(new Spot(3,"Spot vong zwei", 0));

        spotDao.insertAll(new Spot(5,"Sparda einfach", 0));
        spotDao.insertAll(new Spot(6,"Sparda normal", 0));
        spotDao.insertAll(new Spot(7,"Sparda schwer", 0));
        spotDao.insertAll(new Spot(7,"Sparda schwererer", 0));

        spotDao.insertAll(new Spot(2,"p1 Kacke", 0));
        spotDao.insertAll(new Spot(2,"p1 Pimmel", 4));
        spotDao.insertAll(new Spot(2,"p1 Lambda", 24));
        spotDao.insertAll(new Spot(2,"p1 Pi", 68));
        spotDao.insertAll(new Spot(2,"p1 Oreo", 122));
        spotDao.insertAll(new Spot(2,"p1 Lollipop", 253));
        spotDao.insertAll(new Spot(2,"p1 Stift", 255));
        spotDao.insertAll(new Spot(2,"p1 Lampe", 0));
        spotDao.insertAll(new Spot(2,"p1 hallo Oma", 0));
        spotDao.insertAll(new Spot(2,"p1 PS4", 0));
        spotDao.insertAll(new Spot(2,"p1 hoi", 0));
        spotDao.insertAll(new Spot(2,"p1 huch", 0));
        spotDao.insertAll(new Spot(2,"p1 Dreams", 0));
        spotDao.insertAll(new Spot(2,"p1 vong spielerlebnis her unübertreffbar nicht?", 0));
    }

    private void resetRecommendations() {
        RecommendationDao recommendationDao = instance.recommendationDao();
        recommendationDao.deleteAll();
        recommendationDao.insertAll(new Recommendation("Thinking about the Aux Castle Shizzlers.", "In Gedanken bei den Aux Castle Shizzlern.", true, 10));
        recommendationDao.insertAll(new Recommendation("Yearning for the Bois and Grils from Aux Castle.", "Im Herzen bei den Bois und Grils von Aux Castle.", true, 10));
        recommendationDao.insertAll(new Recommendation("Take a train to Augsburg straight after.", "Danach direkt in einen Zug nach Augsburg steigen.", true, 10));
        recommendationDao.insertAll(new Recommendation("Without pants.", "Ohne Hose.", true, 5));
        recommendationDao.insertAll(new Recommendation("With your nose itching.", "Mit juckender Nase.", true, 5));
        recommendationDao.insertAll(new Recommendation("Eating a banana.", "Eine Banane essend.", true, 5));
        recommendationDao.insertAll(new Recommendation("Entering the Church of the Biceps afterwards.", "Ehrfürchtig dem Herrn des Bizeps danken.", true, 5));
        recommendationDao.insertAll(new Recommendation("No Radler, just this once.", "Ausnahmsweise ohne Radler.", true, 5));
        recommendationDao.insertAll(new Recommendation("Using all of your neck.", "Mit vollem Nackeneinsatz.", true, 5));
        recommendationDao.insertAll(new Recommendation("Give a compliment to someone after every move.", "Nach jedem Move jemandem ein Kompliment machen.", true, 10));
        recommendationDao.insertAll(new Recommendation("Use only one leg.", "Alles auf nur einem Bein."));
        recommendationDao.insertAll(new Recommendation("Use only one hand.", "Alles mit nur einer Hand."));
        recommendationDao.insertAll(new Recommendation("Do everything on your bad side.", "Alles nur auf der schlechteren Seite."));
        recommendationDao.insertAll(new Recommendation("Do everything backwards.", "Alles rückwärts machen."));
        recommendationDao.insertAll(new Recommendation("With closed eyes.", "Mit geschlossenen Augen."));
        recommendationDao.insertAll(new Recommendation("Add unnecessary spins.", "Mit unnötig vielen Drehungen."));
        recommendationDao.insertAll(new Recommendation("Keep your eyes on one spot.", "Den Blick auf eine Sache fixiert halten."));
        recommendationDao.insertAll(new Recommendation("Move extremely silently.", "Ohne Geräusche zu machen."));
        recommendationDao.insertAll(new Recommendation("With only one breath.", "In nur einem Atemzug."));
    }



    private void resetMoves() {
        MoveDao moveDao = instance.moveDao();
        moveDao.deleteAll();
        //Auto Increment ID (from table moves) starts at 1, not at 0
        moveDao.insertAll(new Move(1, 1, "Animal Walk", "Animal Walk"));
        moveDao.insertAll(new Move(1, 1, "Cartwheel", "Rad", "#FF0000"));
        moveDao.insertAll(new Move(1, 6, "Hang", "Hängen", "#00FF00"));
        moveDao.insertAll(new Move(1, 1, "High Straight Jump", "Strecksprung", "#FF0000"));
        moveDao.insertAll(new Move(1, 1, "Jog a lap around the spot", "Eine Runde Joggen", "#00FF00"));
        moveDao.insertAll(new Move(1, 1, "Monkey Step", "Monkey Step", "#0000FF"));
        moveDao.insertAll(new Move(1, 4, "Monkey Step under Rail", "Monkey Step unterm Geländer"));
        moveDao.insertAll(new Move(1, 7, "Muscle-Up", "Muscle-Up", "#FF0000"));
        moveDao.insertAll(new Move(1, 7, "Pull-Up", "Klimmzug", "#00FF00"));
        moveDao.insertAll(new Move(1, 1, "Push-Up", "Liegestütz", "#0000FF"));
        moveDao.insertAll(new Move(1, 1, "Raupe", "Raupe", "cyan"));
        moveDao.insertAll(new Move(1, 1, "Short Sprint", "Kurzer Sprint"));
        moveDao.insertAll(new Move(1, 1, "Squat", "Kniebeuge"));
        moveDao.insertAll(new Move(2, 6, "Arm Jump", "Armsprung", "fuchsia"));
        moveDao.insertAll(new Move(2, 3, "Basic Vault", "Basic Vault", "lime"));
        moveDao.insertAll(new Move(2, 6, "Climb-Up", "Climb-Up", "magenta"));
        moveDao.insertAll(new Move(2, 3, "Demi-Tour", "Demi-Tour"));
        moveDao.insertAll(new Move(2, 3, "Fake Reverse", "Fake Reverse"));
        moveDao.insertAll(new Move(2, 3, "Lazy", "Lazy"));
        moveDao.insertAll(new Move(2, 4, "Pass under Bar", "Unterm Geländer durch"));
        moveDao.insertAll(new Move(2, 2, "Precision Jump", "Präzi"));
        moveDao.insertAll(new Move(2, 3, "Reverse", "Reverse"));
        moveDao.insertAll(new Move(2, 1, "Roll Backwards", "Rückwärtsrolle"));
        moveDao.insertAll(new Move(2, 3, "Sit Dash", "Sitzdash"));
        moveDao.insertAll(new Move(2, 3, "Speed Vault", "Speed Vault"));
        moveDao.insertAll(new Move(2, 4, "Underbar", "Durchbruch"));
        moveDao.insertAll(new Move(2, 6, "Wallrun", "Wallrun"));
        moveDao.insertAll(new Move(3, 2, "180 Precision", "180 Präzi"));
        moveDao.insertAll(new Move(3, 3, "Armjump to Rail", "Armsprung ans Geländer"));
        moveDao.insertAll(new Move(3, 3, "Backwards Lazy", "Rückwärts Lazy"));
        moveDao.insertAll(new Move(3, 3, "Cat", "Katze"));
        moveDao.insertAll(new Move(3, 3, "Dash", "Dash"));
        moveDao.insertAll(new Move(3, 8, "Dynamo", "Dynamo"));
        moveDao.insertAll(new Move(3, 3, "Gate Vault", "Gate Vault"));
        moveDao.insertAll(new Move(3, 3, "Jump over Rail", "Sprung übers Geländer"));
        moveDao.insertAll(new Move(3, 7, "Lache", "Lache"));
        moveDao.insertAll(new Move(3, 3, "Lazy Back", "Lazy zurück"));
        moveDao.insertAll(new Move(3, 3, "Lazy over Butt", "Lazy übern Po"));
        moveDao.insertAll(new Move(3, 6, "Return", "Return"));
        moveDao.insertAll(new Move(3, 1, "Roll", "Rolle"));
        moveDao.insertAll(new Move(3, 3, "Speed Step", "Speed Step"));
        moveDao.insertAll(new Move(3, 3, "Support Get-Up", "Aufschwinger"));
        moveDao.insertAll(new Move(4, 3, "180 Get-Up", "180 Aufsprung"));
        moveDao.insertAll(new Move(4, 2, "360 Precision", "360 Präzi"));
        moveDao.insertAll(new Move(4, 3, "540 Lazy", "540 Lazy"));
        moveDao.insertAll(new Move(4, 3, "Almost Lazy", "Fast Lazy"));
        moveDao.insertAll(new Move(4, 3, "Backwards Speed Step", "Rückwärts Speed Step"));
        moveDao.insertAll(new Move(4, 7, "Butt-Swing", "Poschwinger"));
        moveDao.insertAll(new Move(4, 3, "Dive Roll", "Flugrolle"));
        moveDao.insertAll(new Move(4, 8, "Dynamo Down", "Dynamo runter"));
        moveDao.insertAll(new Move(4, 3, "Lazy into Demi-Tour", "Lazy in Demi-Tour"));
        moveDao.insertAll(new Move(4, 8, "Polio", "Polio"));
        moveDao.insertAll(new Move(4, 3, "Rail Fall-Back", "Geländer-Zurückfallen"));
        moveDao.insertAll(new Move(4, 3, "Rail-Sit Get-Up", "Aufsitzer"));
        moveDao.insertAll(new Move(4, 3, "Side Jump over Rail", "Seitsprung übers Geländer"));
        moveDao.insertAll(new Move(4, 2, "Strides", "Strides"));
        moveDao.insertAll(new Move(4, 3, "Thief Vault", "Thief Vault"));
        moveDao.insertAll(new Move(5, 3, "180 Get-Up Speed Step", "180 Aufsprung Speed Step"));
        moveDao.insertAll(new Move(5, 3, "180 Get-Up Spin down", "180 Aufsprung Spin down"));
        moveDao.insertAll(new Move(5, 3, "360 Lazy", "360 Lazy"));
        moveDao.insertAll(new Move(5, 3, "Animal Walk on Rail", "Animal Walk aufm Geländer"));
        moveDao.insertAll(new Move(5, 3, "Barspin", "Barspin"));
        moveDao.insertAll(new Move(5, 3, "Barspin into 360 Lazy", "Barspin in 360 Lazy"));
        moveDao.insertAll(new Move(5, 3, "Barspin into Hook", "Barspin in Hook"));
        moveDao.insertAll(new Move(5, 3, "Barspin into Sit", "Barspin ins Sitzen"));
        moveDao.insertAll(new Move(5, 3, "Barspin into Support", "Barspin in Stütz"));
        moveDao.insertAll(new Move(5, 3, "Cartwheel over Rail", "Rad übers Geländer"));
        moveDao.insertAll(new Move(5, 3, "Cartwheel Rail Boost", "Rad mit Geländer-Boost"));
        moveDao.insertAll(new Move(5, 3, "Cartwheel Rail Leg-Spin", "Rad mit Geländer-Bein-Spin"));
        moveDao.insertAll(new Move(5, 3, "Fake Reverse Frisbee", "Fake Reverse Frisbee"));
        moveDao.insertAll(new Move(5, 4, "Glide Underbar", "Gleitdurchbruch"));
        moveDao.insertAll(new Move(5, 3, "Helikoptero", "Helikoptero"));
        moveDao.insertAll(new Move(5, 3, "Helikoptero to Rail-Lean Tornado-Kick 2", "Helikoptero zu Geländer-Tornado 2"));
        moveDao.insertAll(new Move(5, 4, "Hook Fall", "Hook Fall"));
        moveDao.insertAll(new Move(5, 3, "Hook Spin", "Hook Spin"));
        moveDao.insertAll(new Move(5, 3, "Hook to Sit", "Hook zu Sitzen"));
        moveDao.insertAll(new Move(5, 3, "Hook to Support", "Hook zu Stütz"));
        moveDao.insertAll(new Move(5, 3, "Koala", "Koala"));
        moveDao.insertAll(new Move(5, 3, "Lazy Back to Pushup-Landing", "Lazy zurück zu Liegestütz-Landung"));
        moveDao.insertAll(new Move(5, 3, "Lazy Spin", "Lazy Spin"));
        moveDao.insertAll(new Move(5, 3, "Lazy Twist-In", "Lazy Eindreher"));
        moveDao.insertAll(new Move(5, 4, "Lurchbruch", "Lurchbruch"));
        moveDao.insertAll(new Move(5, 3, "Monkey Step on Rail", "Monkey Step aufm Geländer"));
        moveDao.insertAll(new Move(5, 3, "Rail-Floor Handspring", "Geländer-Boden Handstandüberschlag"));
        moveDao.insertAll(new Move(5, 3, "Rail-Floor Handspring and Back", "Geländer-Boden Handstandüberschlag und zurück"));
        moveDao.insertAll(new Move(5, 3, "Rail-Lean Tornado-Kick 2", "Geländer-Tornado 2"));
        moveDao.insertAll(new Move(5, 3, "Rail-Lean Tornado-Kick 3", "Geländer-Tornado 3"));
        moveDao.insertAll(new Move(5, 3, "Rail Slide", "Geländerslide"));
        moveDao.insertAll(new Move(5, 4, "Rail Spin-through", "Durchdrehen"));
        moveDao.insertAll(new Move(5, 4, "Rail Spin-through to Get-Up", "Durchdrehen zu Aufschwinger"));
        moveDao.insertAll(new Move(5, 3, "Reverse into Demi-Tour", "Reverse in Demi-Tour"));
        moveDao.insertAll(new Move(5, 3, "Roll over Rail", "Rolle übers Geländer"));
        moveDao.insertAll(new Move(5, 3, "Sit to 360-Out", "Sitzen zu 360-Abgang"));
        moveDao.insertAll(new Move(5, 1, "Spanish Glide", "Spanier-Gleiten"));
        moveDao.insertAll(new Move(5, 1, "Spanish Corner Glide", "Spanier-Eckgleiten"));
        moveDao.insertAll(new Move(5, 3, "Spin next to the Rail", "Dreher am Geländer"));
        moveDao.insertAll(new Move(5, 4, "Spinning Support Get-Up", "Beindreher Aufschwung"));
        moveDao.insertAll(new Move(5, 3, "Speed Step Fake", "Speed Step Fake"));
        moveDao.insertAll(new Move(5, 3, "Speed Step Fake x2", "Speed Step Fake x2"));
        moveDao.insertAll(new Move(5, 3, "Support Demi-Tour", "Stütz Demi-Tour"));
        moveDao.insertAll(new Move(5, 3, "Support Demi-Tour into Support", "Stütz Demi-Tour in Stütz"));
        moveDao.insertAll(new Move(5, 3, "Support Barspin", "Stütz Barspin"));
        moveDao.insertAll(new Move(5, 3, "Support Reverse", "Stütz Reverse"));
        moveDao.insertAll(new Move(5, 3, "Support 180 Get-Up", "Stütz 180 Aufschwinger"));
        moveDao.insertAll(new Move(5, 3, "Thief Twist-In", "Thief Eindreher"));
        moveDao.insertAll(new Move(5, 3, "Thigh Rail-Spin to Sit", "Schenkel-Drehsitzer"));
        moveDao.insertAll(new Move(6, 1, "B-Kick Groundroll", "B-Kick Bodenrolle"));
        moveDao.insertAll(new Move(6, 1, "B-Twist", "B-Twist"));
        moveDao.insertAll(new Move(6, 1, "Backflip", "Backflip"));
        moveDao.insertAll(new Move(6, 1, "Frontflip", "Frontflip"));
        moveDao.insertAll(new Move(6, 7, "Half Swing Gainer", "Halber Swing Gainer"));
        moveDao.insertAll(new Move(6, 1, "Handspring", "Handstandüberschlag"));
        moveDao.insertAll(new Move(6, 1, "Kick-Up", "Kick-Up"));
        moveDao.insertAll(new Move(6, 3, "Rail Handspring", "Geländer-Handstandüberschlag"));
        moveDao.insertAll(new Move(6, 1, "Sideflip", "Sideflip"));
        moveDao.insertAll(new Move(6, 7, "Swing Gainer", "Swing Gainer"));
        moveDao.insertAll(new Move(6, 1, "Tornado Kick", "Tornado Kick"));
        moveDao.insertAll(new Move(6, 4, "Wallspin", "Wallspin"));
        moveDao.insertAll(new Move(6, 1, "Webster", "Webster"));
        moveDao.insertAll(new Move(7, 1, "Arabian", "Arabian"));
        moveDao.insertAll(new Move(7, 1, "Back Full", "Back Full"));
        moveDao.insertAll(new Move(7, 1, "Front 180", "Front 180"));
        moveDao.insertAll(new Move(7, 1, "Front Full", "Front Full"));
        moveDao.insertAll(new Move(7, 1, "Raiz", "Raiz"));
        moveDao.insertAll(new Move(7, 1, "Touchdown Raiz", "Touchdown Raiz"));
        moveDao.insertAll(new Move(8, 1, "B-Kick", "B-Kick"));
        moveDao.insertAll(new Move(8, 1, "Ballerina", "Ballerina"));
        moveDao.insertAll(new Move(8, 1, "Fake Flip", "Fake Flip"));
        moveDao.insertAll(new Move(8, 1, "Fake Cork", "Fake Cork"));
        moveDao.insertAll(new Move(8, 1, "Get-Up Moon Kick", "Aufsteher Mondkick"));
        moveDao.insertAll(new Move(8, 1, "Ground Bug-Spin", "Boden-Käferspin"));
        moveDao.insertAll(new Move(8, 3, "Lazy Fake", "Lazy Fake"));
        moveDao.insertAll(new Move(8, 3, "Lazy Fake Scoot", "Lazy Fake Scoot"));
        moveDao.insertAll(new Move(8, 1, "Pose", "Rumposen"));
        moveDao.insertAll(new Move(8, 1, "Represent", "Represent"));
    }

    private void resetTipps() {
        TippDao tippDao = instance.tippDao();
        tippDao.deleteAll();
        //Auto Increment ID (from table moveStyleOrDifficulty) starts at 1, not at 0
        String[][] tippDescriptions = {
                {"Let friends use this app on a different profile to keep your settings safe.", "Lasse deine Freunde auf einem anderen Profil an die App, um deine Einstellungen zu sichern."},
                {"A video showing all preinstalled moves is linked in the Help menu. Tutorials soon?", "Link zu Video mit allen vorinstallierten Moves gibt's im Hilfemenü. Tutorials auch bald?"},
                {"Add your own moves in the Move Edit menu.", "Erstelle deine eigenen Moves im Menü Moves Bearbeiten."},
                {"Add moves to a spot in the Spots menu.", "Füge Moves im Spotmenü zu einem Spot hinzu."},
                {"Moves will only appear in the Combo Generator if selected in the current spot.", "Moves werden nur in einer Combo generiert, wenn sie im aktuellen Spot ausgewählt sind."},
                {"Moves can exist on their own but are connected to spots, and spots are connected to profiles.", "Moves existieren für sich, sind aber verbunden mit Spots, und Spots mit Profilen."},
                {"Moves can be translated into all supported languages when creating them.", "Beim Erstellen von Moves kannst du alle unterstützen Sprachen eintragen."},
                {"Spots can be copied and moved between profiles.", "Spots können zwischen Profilen kopiert und verschoben werden."},
                {"Editing a move will change it all over the app.", "Änderst du einen Move, wird er in der ganzen App verändert."},
                {"Deleting a move removes it from every spot and profile.", "Löschst du einen Move, verschwindet er von allen Spots und Profilen."},
                {"You can turn off tipps in the settings.", "Tipps lassen sich in den Einstellungen ausschalten."},
                {"Most settings are saved in the current profile.", "Einstellungen sind meistens ans gewählte Profil gebunden."},
                {"Every profile remembers its last used color theme.", "Jedes Profil speichert sein eigenes Farbschema."},
                {"The Night Mode can't be changed.", "Der Nachtmodus ist nicht veränderbar."},
                {"Can't see the color theme change? Try turning off Night Mode.", "Farbschemawechsel nicht sichtbar? Nachtmodus ausmachen!"},
                {"Most color themes are inspired by great open source apps.", "Die meisten Farbschemata sind inspiriert von Freier Software."},
                {"Some settings allow more interactivity between profiles. Very meta.", "Manche Einstellungen erlauben mehr Interaktivität zwischen Profilen. Sehr meta."},
                {"Some settings activate shortcuts to functions that have been deemed cluttering or confusing.", "Manche Einstellungen aktivieren Abkürzungen zu Funktionen, welche für eine bessere Übersicht entfernt wurden."},
                {"Remember to drill the basics. Get 20 repetitions of an easy move straight after warm up.", "Basics Drillen nicht vergessen, z.B. 20 Wiederholungen eines leichten Moves direkt nachm Aufwärmen."},
                {"Don't let this app replace your own creativity and intentionally crafted combos.", "Lass diese App nicht deine eigene Kreativität und mühevoll konstruierte Combos ersetzen."},
                {"Randomness has resulted in many beautiful discoveries.", "Zufall hat zu vielen schönen Entdeckungen geführt."},
                {"Take a friend, generate a combo and see who can complete it first!", "Zu zweit eine Combo generieren und schauen, wer sie zuerst komplett hinbekommt."},
                {"Interested in translating this app? It's really easy! See the Help menu for more info.", "Du willst diese App übersetzen? Ist ganz einfach. Mehr Infos im Hilfemenü."},
                {"Any ideas for improving this app? Contact info is in the Help menu.", "Verbesserungsideen? Kontaktinfos findest du im Hilfemenü."},
                {"This app was inspired in part by Minh's Shine and its effect on the programmers' training style.", "Diese App wurde u.a. inspiriert durch Minhs Shine und seinen Einfluss auf den Trainingsstil der Programmierer dieser App."},
                {"This app was inspired in part by the sheer lack of Spots without rails in the programmers' home town.", "Diese App wurde u.a. inspiriert durch den Mangel an Spots ohne Geländer in der Heimatstadt der Programmierer dieser App."},
                {"This app was programmed in android studio using java.", "Diese App wurde in Android Studio mit Java programmiert."},
                {"This app is open source. A link to the source code is in the Help menu.", "Diese App ist freie Software. Link zum Code im Hilfemenü."},
                {"Huge thanks to John's Android Studio Tutorials, Coding in Flow and everyone on Stackexchange.", "Tausend Dank an John's Android Studio Tutorials, Coding in Flow und alle bei Stackexchange."}
        };
        for (int i = 0; i < tippDescriptions.length; i++) {
            tippDao.insertAll(new Tipp(tippDescriptions[i][0], tippDescriptions[i][1]));
        }

    }

    private void resetMoveStyleOrDifficulty() {
        MoveStyleOrDifficultyDao moveStyleOrDifficultyDao = instance.moveStyleOrDifficultyDao();
        moveStyleOrDifficultyDao.deleteAll();
        //Auto Increment ID (from table moveStyleOrDifficulty) starts at 1, not at 0
        String[][] difficultyDescriptions = {
                {"Basic Moves", "Normale Moves", "#33FF0000"},
                {"Pk Basics", "Pk Grundlagen", "#00000000"},
                {"Pk Advanced", "Pk Könner", "#330000FF"},
                {"Pk Expert", "Pk Profi", "#33FFFF00"},
                {"Technical/Flow", "Kniffliges", "#3300FF00"},
                {"FR Basics", "FR Grundlagen", "#00000000"},
                {"FR Advanced", "FR Profi", "#33FF00FF"},
                {"0% Injury Style", "0% Gefahr Style", "#3300FFFF"},
        };
        for (int i = 0; i < difficultyDescriptions.length; i++) {
            moveStyleOrDifficultyDao.insertAll(new MoveStyleOrDifficulty(difficultyDescriptions[i][0], difficultyDescriptions[i][1], difficultyDescriptions[i][2], false));
        }

    }

    private void resetMoveSpotTypeNeeded () {
        MoveSpotTypeNeededDao moveSpotTypeNeededDao = instance.moveSpotTypeNeededDao();
        moveSpotTypeNeededDao.deleteAll();
        //Auto Increment ID (from table moveSpotTypeNeeded) starts at 1, not at 0

        String[][] spotTypeDescriptions = {
                {"Ground", "Boden", "#33FFFF00"},
                {"Precision", "Präzi", "#33FF00FF"},
                {"Hip-height", "Hüfthoch", "#3300FFFF"},
                {"Rail", "Geländer", "#00000000"},
                {"Double Bar", "Doppelstangen", "#330000FF"},
                {"High Wall", "Hohe Mauer", "#00000000"},
                {"High Bar", "Hohe Stange", "#33FF0000"},
                {"Special", "Spezial", "#3300FF00"},
        };
        for (int i = 0; i < spotTypeDescriptions.length; i++) {
            moveSpotTypeNeededDao.insertAll(new MoveSpotTypeNeeded(spotTypeDescriptions[i][0], spotTypeDescriptions[i][1], spotTypeDescriptions[i][2], false));
        }

    }

    private void resetMovementTypes() {
        MovementTypeDao movementTypeDao = instance.movementTypeDao();
        movementTypeDao.deleteAll();
        movementTypeDao.insertAll(new MovementType("Parkour", "Parkour", true));
        movementTypeDao.insertAll(new MovementType("Juggling", "Jonglieren", true));
        movementTypeDao.insertAll(new MovementType("Dancing", "Tanzen", true));
    }

    private void resetCurrentCombos() {
        CurrentComboDao currentComboDao = instance.currentComboDao();
        currentComboDao.deleteAll();
    }



    public abstract ProfileDao profileDao();
    public abstract SpotDao spotDao();
    public abstract MoveDao moveDao();
    public abstract RecommendationDao recommendationDao();
    public abstract TippDao tippDao();
    public abstract MoveStyleOrDifficultyDao moveStyleOrDifficultyDao();
    public abstract MoveSpotTypeNeededDao moveSpotTypeNeededDao();
    public abstract MovementTypeDao movementTypeDao();

    public abstract MoveSpotJoinDao moveSpotJoinDao();
    public abstract RecommendationProfileJoinDao recommendationProfileJoinDao();

    public abstract CurrentComboDao currentComboDao();

    public abstract HistoryDao historyDao();
    public abstract FavoriteDao favoriteDao();

}