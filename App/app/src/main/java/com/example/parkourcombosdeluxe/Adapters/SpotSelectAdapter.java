package com.example.parkourcombosdeluxe.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.ItemsForRecyclerView.SpotSelectItem;
import com.example.parkourcombosdeluxe.R;

import java.util.ArrayList;

public class SpotSelectAdapter extends RecyclerView.Adapter<SpotSelectAdapter.SpotSelectViewHolder> {

    private ArrayList<SpotSelectItem> mSpotSelectList;
    private OnItemClickListener mListener;
    private OnItemLongClickListener mLongClickListener;
    private Context mContext;
    private int position;

    public interface OnItemClickListener {
        public void onItemClicked(int position);
    }
    public void setItemOnClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemLongClickListener {
        public boolean onItemLongClicked(int position);
    }
    public void setItemOnLongClickListener(OnItemLongClickListener LongClickListener) {
        mLongClickListener = LongClickListener;
    }

    private Fragment mFragment;


    public static class SpotSelectViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mTextTitle;
        public TextView mTextDescr;
        public CardView spotSelectCardView;

        public SpotSelectViewHolder(@NonNull View itemView, final OnItemClickListener listener, final OnItemLongClickListener longClickListener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.spotSelectItemImageView);
            mTextTitle = itemView.findViewById(R.id.spotSelectItemTitleText);
            mTextDescr = itemView.findViewById(R.id.spotSelectItemDescrText);
            spotSelectCardView = itemView.findViewById(R.id.spotSelectCardView);

/*            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (longClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            longClickListener.onItemLongClick(position);
                        }
                    }
                    return false;
                }
            });*/


        }


    }

    public SpotSelectAdapter(ArrayList<SpotSelectItem> spotSelectList, Context mContext) {
        this.mSpotSelectList = spotSelectList;
        this.mContext = mContext;

    }

    @NonNull
    @Override
    public SpotSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spot_select, parent, false);
        SpotSelectViewHolder ssvh = new SpotSelectViewHolder(v, mListener, mLongClickListener);
        return ssvh;
    }

    //Design the cards layout
    @Override
    public void onBindViewHolder(@NonNull final SpotSelectViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClicked(position);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mLongClickListener.onItemLongClicked(position);
                return true;
            }
        });

        SpotSelectItem currentItem = mSpotSelectList.get(position);

        int currentCombosDoneCount = currentItem.getSpotCombosDoneCount();
        if (currentCombosDoneCount >= 255) {
            holder.mImageView.setColorFilter(mContext.getResources().getColor(R.color.colorSpot4));
            //holder.mImageView.setImageResource(R.drawable.icon_spot_colorspot4));
        } else if (currentCombosDoneCount >= 123) {
            holder.mImageView.setColorFilter(mContext.getResources().getColor(R.color.colorRedFavorite));
        } else if (currentCombosDoneCount >= 69) {
            holder.mImageView.setColorFilter(mContext.getResources().getColor(R.color.colorSpot3));
        } else if (currentCombosDoneCount >= 25) {
            holder.mImageView.setColorFilter(mContext.getResources().getColor(R.color.colorSpot2));
        } else if (currentCombosDoneCount >= 5) {
            holder.mImageView.setColorFilter(mContext.getResources().getColor(R.color.colorSpot1));
        } else if (currentCombosDoneCount > 0) {
            holder.mImageView.setColorFilter(mContext.getResources().getColor(R.color.colorGreenCheck));
        } else {
            holder.mImageView.setColorFilter(mContext.getResources().getColor(R.color.colorSpot0));
        }


        holder.mTextTitle.setText(currentItem.getSpotName());
        holder.mTextDescr.setText(String.valueOf(currentItem.getSpotCombosDoneCount()));







        /*holder.spotSelectCardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.spotSelectCardView);
                //inflating menu from xml resource
                popup.inflate(R.menu.floating_spot_select);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.floating_spot_select:
                                toastMessage("select");
                                break;
                            case R.id.floating_spot_rename:
                                toastMessage("rename");
                                break;
                            case R.id.floating_spot_moves:
                                toastMessage("moves");
                                break;
                            case R.id.floating_spot_migrate:
                                toastMessage("migrate");
                                break;
                            case R.id.floating_spot_color:
                                toastMessage("color");
                                break;
                            case R.id.floating_spot_delete:
                                toastMessage("delete");
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

                return true;
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return mSpotSelectList.size();
    }

    public int getPosition() { return position; }

    public void setPosition(int position) {
        this.position = position;
    }

    private void toastMessage (String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
}
