package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface HistoryDao {

    //TODO: (here and everywhere else): make this sort by ID, if necessary
    @Query("SELECT * FROM history")
    List<History> getAll();

    @Query("SELECT * FROM history where ID LIKE  :ID")
    History findByID(Integer ID);

    @Insert
    void insertAll(History... historys);

    @Update
    void updateHistorys(History... historys);

    @Delete
    void delete(History history);

    @Query("DELETE FROM history")
    void deleteAll();
}