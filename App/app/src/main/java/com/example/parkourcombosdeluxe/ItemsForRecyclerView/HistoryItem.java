package com.example.parkourcombosdeluxe.ItemsForRecyclerView;

public class HistoryItem {
    private int mImageResource;
    private String mTextTitle;
    private String mTextDescr;

    public HistoryItem(int imageResource, String textTitle, String textDescr) {
        mImageResource = imageResource;
        mTextTitle = textTitle;
        mTextDescr = textDescr;
    }

    public int getImageResource() {
        return mImageResource;
    }

    public String getTextTitle() {
        return mTextTitle;
    }

    public String getTextDescr() {
        return mTextDescr;
    }

}


