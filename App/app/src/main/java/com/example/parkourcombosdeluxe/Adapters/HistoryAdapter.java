package com.example.parkourcombosdeluxe.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parkourcombosdeluxe.ItemsForRecyclerView.SpotSelectItem;
import com.example.parkourcombosdeluxe.R;

import java.util.ArrayList;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.SpotSelectViewHolder> {

    private ArrayList<SpotSelectItem> mSpotSelectList;
    private OnItemClickListener mListener;


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setItemOnClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public static class SpotSelectViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mTextTitle;
        public TextView mTextDescr;

        public SpotSelectViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.spotSelectItemImageView);
            mTextTitle = itemView.findViewById(R.id.spotSelectItemTitleText);
            mTextDescr = itemView.findViewById(R.id.spotSelectItemDescrText);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public HistoryAdapter(ArrayList<SpotSelectItem> spotSelectList) {
        mSpotSelectList = spotSelectList;

    }

    @NonNull
    @Override
    public SpotSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spot_select, parent, false);
        SpotSelectViewHolder ssvh = new SpotSelectViewHolder(v, mListener);
        return ssvh;
    }

    @Override
    public void onBindViewHolder(@NonNull SpotSelectViewHolder holder, int position) {
        SpotSelectItem currentItem = mSpotSelectList.get(position);

/*        holder.mImageView.setImageResource(currentItem.getImageResource());
        holder.mTextTitle.setText(currentItem.getTextTitle());
        holder.mTextDescr.setText(currentItem.getTextDescr());*/
    }

    @Override
    public int getItemCount() {
        return mSpotSelectList.size();
    }
}
