package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class CurrentCombo {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    public Integer moveID;
    public String moveName;

    //TODO: Make History table clear itself. Limit entries by 30 or so

    public CurrentCombo(@NonNull Integer moveID, String moveName) {
        this.moveID = moveID;
        this.moveName = moveName;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getMoveID() {
        return moveID;
    }
    public void setMoveID(Integer moveID) {
        this.moveID = moveID;
    }

    public String getMoveName() {
        return moveName;
    }
    public void setMoveName(@NonNull String moveName) {
        this.moveName = moveName;
    }
}