package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class History {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    @NonNull
    public String content;
    public Boolean done;
    public Boolean fav;
    public String timestamp;
    public Integer profileID;
    public String profileName;


    //TODO: Make History table clear itself. Limit entries by 30 or so

    public History(@NonNull String content, Boolean done, Boolean fav, String timestamp, Integer profileID, String profileName) {
        this.content = content;
        this.done = done;
        this.fav = fav;
        this.timestamp = timestamp;
        this.profileID = profileID;
        this.profileName = profileName;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    @NonNull
    public String getContent() {
        return content;
    }
    public void setContent(@NonNull String content) {
        this.content = content;
    }

    public Boolean getDone() {
        return done;
    }
    public void setDone(Boolean done) {
        this.done = done;
    }

    public Boolean getFav() {
        return fav;
    }
    public void setFav(Boolean fav) {
        this.fav = fav;
    }

    public String getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getProfileID() {
        return profileID;
    }
    public void setProfileID(Integer profileID) {
        this.profileID = profileID;
    }

    public String getProfileName() {
        return profileName;
    }
    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

}