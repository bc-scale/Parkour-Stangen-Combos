package com.example.parkourcombosdeluxe.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Adapters.SpotSelectAdapter;
import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.ProfileDao;
import com.example.parkourcombosdeluxe.Data.Spot;
import com.example.parkourcombosdeluxe.Data.SpotDao;
import com.example.parkourcombosdeluxe.ItemsForRecyclerView.SpotSelectItem;
import com.example.parkourcombosdeluxe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpotSelectActivity extends BaseActivity {

    /*@BindView(R.id.layout_SpotSelectScroll)
    RecyclerView.Recycler recyclerView;*/
    @BindView(R.id.spotSelectStatusText)
    TextView spotSelectStatusText;

    private ArrayList<SpotSelectItem> mSpotSelectList;

    private RecyclerView mRecyclerView;
    private SpotSelectAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    List<Spot> currentProfileSpots;

    boolean noSpotForCurrentProfile = false;
    String tempSpotNameFromDiscard = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spot_select);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tempSpotNameFromDiscard = getResources().getString(R.string.spot_name);

        //DBs
        spotDao = AppDatabase.getInstance(this).spotDao();
        profileDao = AppDatabase.getInstance(this).profileDao();
        currentProfileSpots = spotDao.getAllByProfileID(currentProfile.getID());

        //check current Spot or if there are any
        setCurrentSpotIfThereAreAny();

        generateSpots();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_spot_select, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_spots_help:
                createDialogHelpBasic(SpotSelectActivity.this, getResources().getString(R.string.spots_dialog_help_title), getResources().getString(R.string.spots_dialog_help_text));
                break;
            case R.id.action_spots_add:
                createDialogAddSpot(SpotSelectActivity.this);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }






    //add and remove entries from list (for later with animation without recreate(); :
    // mSpotSelectList.add(new SpotSelectItem(newSpot));
    // mAdapter.notifyDataSetChanged(); <- not what we use (refreshes whole list, no animation), use rather one of these two:
    // mAdapter.notifyItemInserted(position); <- position indicates where to put it in, has animation, just put at end
    // mAdapter.notifyItemRemoved(position); <- position where to delete, take proper position or rekt

    private void updateSpotSelectStatusText() {
        if (noSpotForCurrentProfile) {
            spotSelectStatusText.setText(getResources().getString(R.string.spots_status_no_existing_spots));
        } else {
            spotSelectStatusText.setText(getResources().getString(R.string.spots_title_status)
                    + "\n" + currentProfile.getName()
                    + ", " + currentSpot.getName() );
        }
    }

    private void generateSpots() {
        mSpotSelectList = new ArrayList<>();
        updateSpotSelectStatusText();

        if (!noSpotForCurrentProfile) {
            List<Spot> spots = spotDao.getAllByProfileID(currentProfile.getID());
            final int currentSpotId = currentSpot.getID();

            //CAREFUL: i should be one smaller than spots.size() in the last loop, as i is used in spots.get(i), where counting starts from 0
            for (int i = 0; i < spots.size(); i++) {
                mSpotSelectList.add(new SpotSelectItem(spots.get(i)));
                buildRecyclerView();
            }
        }
    }


    public void createDialogEditSpot(Context context) {

    }




    private void createDialogAddSpot(Context context){
        //final Context contextFinal = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final LinearLayout inputTextLayout = new LinearLayout(SpotSelectActivity.this);
        inputTextLayout.setOrientation(LinearLayout.VERTICAL);

        final EditText inputSpotName = new EditText(context);

        LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        editTextParams.setMargins(50, 0, 50, 0);

        builder.setTitle(getString(R.string.dialog_create_spot_title));

        inputSpotName.setText(tempSpotNameFromDiscard);
        inputTextLayout.addView(inputSpotName, editTextParams);
        builder.setView(inputTextLayout);

        builder.setPositiveButton(getString(R.string.dialog_button_confirm_textedit), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final String newSpotName = inputSpotName.getText().toString();

                if (newSpotName.isEmpty()) {
                    toastMessage(getResources().getString(R.string.toast_spot_name_empty));
                } else {
                    if (checkDoubleSpotNamesInProfile(newSpotName)) {
                        createDialogAddSpotDoubleWarning(newSpotName,SpotSelectActivity.this);
                    } else {
                        Spot newSpot = new Spot(currentProfile.getID(), newSpotName);
                        spotDao.insertAll(newSpot);
                        String message = getResources().getString(R.string.toast_confirm_spot_creation1) + newSpotName + getResources().getString(R.string.toast_confirm_spot_creation2);
                        toastMessage(message);
                        if (noSpotForCurrentProfile) {
                            makeMainRecreate();
                            recreate();
                        } else {
                            insertItemAtEnd(spotDao.getAll().get(spotDao.getAll().size() - 1));
                        }
                    }
                }
            }
        });

        builder.setNegativeButton(getString(R.string.dialog_button_cancel_normal), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        builder.show();
    }

    private Boolean checkDoubleSpotNamesInProfile(String checkSpotName) {
        List<Spot> checkSpots = spotDao.getAllByProfileID(currentProfile.getID());
        for (Spot a : checkSpots) {
            if (a.getName().equalsIgnoreCase(checkSpotName)) {
                return true;
            }
        }
        return false;
    }

    private void createDialogAddSpotDoubleWarning(String doubleName, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String newSpotName = doubleName;

        builder.setTitle(getString(R.string.dialog_warning_title));
        String message = getResources().getString(R.string.dialog_spotadd_double_name_conflict_1) + doubleName + getResources().getString(R.string.dialog_spotadd_double_name_conflict_2);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.dialog_button_confirm_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Spot newSpot = new Spot(currentProfile.getID(), newSpotName);
                spotDao.insertAll(newSpot);
                toastMessage(getResources().getString(R.string.toast_confirm_spot_creation1) + newSpotName + getResources().getString(R.string.toast_confirm_spot_creation2));
                recreate();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_button_cancel_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                tempSpotNameFromDiscard = newSpotName;
                toastMessage(getResources().getString(R.string.toast_discard_add_entry));
            }
        });
        builder.show();
    }




    private void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.spotSeletRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new SpotSelectAdapter(mSpotSelectList, SpotSelectActivity.this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        registerForContextMenu(mRecyclerView);

        mAdapter.setItemOnClickListener(new SpotSelectAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                chooseItem(position);
                // call this when a spot was changed:
            }
        });

        mAdapter.setItemOnLongClickListener(new SpotSelectAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(int position) {
                createPopupMenu(SpotSelectActivity.this, mRecyclerView.findViewHolderForAdapterPosition(position).itemView, position);
                return false;
            }
        });

    }

    public void createPopupMenu(Context popContext, View view, final int position) {
        //creating a popup menu
        PopupMenu popup = new PopupMenu(popContext, view);
        //inflating menu from xml resource
        popup.inflate(R.menu.floating_spot_select);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.floating_spot_moves:
                        toastMessage("moves" + String.valueOf(position));
                        break;
                    case R.id.floating_spot_rename:
                        toastMessage("rename" + String.valueOf(position));
                        break;
                    case R.id.floating_spot_migrate:
                        toastMessage("migrate" + String.valueOf(position));
                        break;
                    case R.id.floating_spot_color:
                        toastMessage("color" + String.valueOf(position));
                        break;
                    case R.id.floating_spot_delete:
                        toastMessage("delete" + String.valueOf(position));
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        //displaying the popup
        popup.show();
    }

    public void chooseItem(int position) {
        if (mSpotSelectList.get(position).getSpotID() != currentSpot.getID()) {
            currentProfile.setCurrentSpotId(mSpotSelectList.get(position).getSpotID());
            profileDao.updateProfiles(currentProfile);
            updateSpotSelectStatusText();
            makeMainRecreate();
            finish();
        }
    }

    public void insertItem(int position, Spot newSpot) {
        mSpotSelectList.add(position, new SpotSelectItem(newSpot));
        mAdapter.notifyItemInserted(position);
        mRecyclerView.scrollToPosition(position);
    }

    public void insertItemAtEnd(Spot newSpot) {
        insertItem(mSpotSelectList.size(), newSpot);
    }

    public void removeItem(int position) {
        //just pass the position, the spot gets deleted here:
        boolean currentSpotDeleted = (mSpotSelectList.get(position).getSpotID() == currentSpot.getID());
        boolean lastSpotDeleted = (mSpotSelectList.size() <= 1);

        spotDao.delete(mSpotSelectList.get(position).getSpot());

        mSpotSelectList.remove(position);
        mAdapter.notifyItemRemoved(position);

        if (currentSpotDeleted || lastSpotDeleted) { recreate(); }
    }

    public void changedItem(int position, Spot changedSpot) {
        //changedSpot should already have been updated into the Database, this only handles the change of the RecyclerView
        boolean currentSpotChanged = (mSpotSelectList.get(position).getSpotID() == currentSpot.getID());

        mSpotSelectList.get(position).updateSpot(changedSpot);
        mAdapter.notifyItemChanged(position);

        if (currentSpotChanged) { updateSpotSelectStatusText(); }
    }

}
