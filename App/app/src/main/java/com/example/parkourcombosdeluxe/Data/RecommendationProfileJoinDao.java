package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface RecommendationProfileJoinDao {

    @Insert
    void insert(RecommendationProfileJoin recommendationProfileJoin);

    @Query("SELECT * FROM recommendation INNER JOIN recommendation_profile_join " +
            "ON recommendation.ID=recommendation_profile_join.recommendationID " +
            " WHERE recommendation_profile_join.profileID=:profileIDIn")
    List<Recommendation> getRecommendationsForProfile(final Integer profileIDIn);

    @Query("SELECT * FROM recommendation INNER JOIN recommendation_profile_join " +
            "ON recommendation.ID=recommendation_profile_join.recommendationID " +
            " WHERE recommendation_profile_join.profileID=:profileIDIn " +
            " AND recommendation_profile_join.weight>:weightLimit")
    List<Recommendation> getRecommendationsForProfileWithMinWeight(final Integer profileIDIn, final Integer weightLimit);

    @Query("SELECT * FROM profile INNER JOIN recommendation_profile_join " +
            "ON profile.ID=recommendation_profile_join.profileID " +
            " WHERE recommendation_profile_join.recommendationID=:recommendationIDIn")
    List<Profile> getProfilesForRecommendation(final Integer recommendationIDIn);

    @Query("DELETE FROM recommendation_profile_join " +
            " WHERE profileID=:profileIDIn")
    void deleteWithProfileId(final Integer profileIDIn);

    @Delete
    void delete(RecommendationProfileJoin recommendationProfileJoin);

    @Query("DELETE FROM recommendation_profile_join")
    void deleteAll();
}