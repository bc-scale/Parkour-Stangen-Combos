package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface FavoriteDao {

    //TODO: (here and everywhere else): make this sort by ID, if necessary
    @Query("SELECT * FROM favorite")
    List<Favorite> getAll();

    @Query("SELECT * FROM favorite where profileID LIKE :profileID")
    List<Favorite> getAllByProfileID(Integer profileID);

    @Query("SELECT * FROM favorite where ID LIKE  :ID")
    Favorite findByID(Integer ID);

    @Query("SELECT MAX(ID) FROM favorite")
    int getNewestID();

    @Insert
    void insertAll(Favorite... favorites);

    @Update
    void updateFavorites(Favorite... favorites);

    @Delete
    void delete(Favorite favorite);

    @Query("DELETE FROM favorite")
    void deleteAll();
}