package com.example.parkourcombosdeluxe.Data;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "recommendation_profile_join",
        primaryKeys = {"recommendationID", "profileID"},
        foreignKeys = {
                @ForeignKey(entity = Recommendation.class,
                        parentColumns = "ID",
                        childColumns = "recommendationID",
                        onDelete = CASCADE),
                @ForeignKey(entity = Profile.class,
                        parentColumns = "ID",
                        childColumns = "profileID",
                        onDelete = CASCADE)
        })
public class RecommendationProfileJoin {
    @NonNull
    public final Integer recommendationID;
    @NonNull
    public final Integer profileID;
    public Integer weight;

    public RecommendationProfileJoin(@NonNull Integer recommendationID, @NonNull Integer profileID) {
        this.recommendationID = recommendationID;
        this.profileID = profileID;
        this.weight = 40;
    }
}