package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Tipp {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    String englishDescr;
    String germanDescr;

    Tipp(String englishDescr, String germanDescr) {
        this.englishDescr = englishDescr;
        this.germanDescr = germanDescr;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getEnglishDescr() {
        return englishDescr;
    }
    public void setEnglishDescr(String englishDescr) {
        this.englishDescr = englishDescr;
    }

    public String getGermanDescr() {
        return germanDescr;
    }
    public void setGermanDescr(String germanDescr) {
        this.germanDescr = germanDescr;
    }


}