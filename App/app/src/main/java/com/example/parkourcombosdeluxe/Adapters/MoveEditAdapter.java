package com.example.parkourcombosdeluxe.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.Move;
import com.example.parkourcombosdeluxe.Data.MoveSpotTypeNeededDao;
import com.example.parkourcombosdeluxe.Data.MoveStyleOrDifficultyDao;
import com.example.parkourcombosdeluxe.ItemsForRecyclerView.MoveEditItem;
import com.example.parkourcombosdeluxe.R;

import java.util.ArrayList;

import static com.example.parkourcombosdeluxe.Activities.MoveEditActivity.actionModeActive;

public class MoveEditAdapter extends RecyclerView.Adapter<MoveEditAdapter.MoveEditViewHolder> {

    private ArrayList<MoveEditItem> mMoveEditList;
    private OnItemClickListener mListener;
    private OnItemLongClickListener mLongClickListener;
    private Context mContext;
    private int position;
    MoveStyleOrDifficultyDao moveStyleOrDifficultyDao;
    MoveSpotTypeNeededDao moveSpotTypeNeededDao;

    public interface OnItemClickListener {
        public void onItemClicked(int position);
        public void onFavoriteClicked(int position);
    }
    public void setItemOnClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemLongClickListener {
        public boolean onItemLongClicked(int position);
    }
    public void setItemOnLongClickListener(OnItemLongClickListener LongClickListener) {
        mLongClickListener = LongClickListener;
    }

    private Fragment mFragment;


    public static class MoveEditViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageViewIcon;
        public ImageView mMoveEditItemImageViewFavorite;
        public CheckBox mMoveEditActionModeCheckBox;
        public TextView mTextMoveName;
        public TextView mTextMoveStyleOrDifficulty;
        public TextView mTextMoveSpotTypeNeeded;
        public TextView mTextUsedAmount;
        public View moveEditBackgroundColorViewLeft;
        public View moveEditBackgroundColorViewRight;
        public CardView moveEditCardView;

        public MoveEditViewHolder(@NonNull View itemView, final OnItemClickListener listener, final OnItemLongClickListener longClickListener) {
            super(itemView);
            mImageViewIcon = itemView.findViewById(R.id.moveEditItemImageViewIcon);
            mTextMoveName = itemView.findViewById(R.id.moveEditItemName);
            mTextMoveStyleOrDifficulty = itemView.findViewById(R.id.moveEditItemStyleOrDifficulty);
            mTextMoveSpotTypeNeeded = itemView.findViewById(R.id.moveEditItemSpotTypeNeeded);
            mTextUsedAmount = itemView.findViewById(R.id.moveEditItemUsedAmount);

            mMoveEditItemImageViewFavorite = itemView.findViewById(R.id.moveEditItemImageViewFavorite);
            mMoveEditActionModeCheckBox = itemView.findViewById(R.id.moveEditCheckBox);

            moveEditBackgroundColorViewLeft = itemView.findViewById(R.id.moveEditBackgroundColorViewLeft);
            moveEditBackgroundColorViewRight = itemView.findViewById(R.id.moveEditBackgroundColorViewRight);

            moveEditCardView = itemView.findViewById(R.id.moveEditCardView);
        }


    }

    public MoveEditAdapter(ArrayList<MoveEditItem> moveEditList, Context mContext) {
        this.mMoveEditList = moveEditList;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public MoveEditViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_move_edit, parent, false);
        MoveEditViewHolder ssvh = new MoveEditViewHolder(v, mListener, mLongClickListener);
        return ssvh;
    }

    //Design the cards layout
    @Override
    public void onBindViewHolder(@NonNull final MoveEditViewHolder holder, final int position) {
        holder.mMoveEditItemImageViewFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFavoriteClicked(position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClicked(position);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mLongClickListener.onItemLongClicked(position);
                return true;
            }
        });





        MoveEditItem currentItem = mMoveEditList.get(position);

        if (currentItem.getIconColor() != null) {
            //int currentMoveIconColorInt = Color.parseColor(currentItem.getIconColor());
            int currentMoveIconColorInt = Color.parseColor(currentItem.getIconColor());
            //test:
            holder.mImageViewIcon.setColorFilter(currentMoveIconColorInt);
            //holder.mImageViewIcon.setColorFilter(currentMoveIconColorInt, android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            //need this, else random colors for some reason
            holder.mImageViewIcon.setColorFilter(null);
        }

        //setting the heart fill:
        if (currentItem.getFavorite()) {
            holder.mMoveEditItemImageViewFavorite.setImageDrawable(mContext.getDrawable(R.drawable.icon_heart_fill));
            holder.mMoveEditItemImageViewFavorite.setColorFilter(mContext.getResources().getColor(R.color.colorRedFavorite));
        } else {
            holder.mMoveEditItemImageViewFavorite.setImageDrawable(mContext.getDrawable(R.drawable.icon_heart_empty));
            //TODO: get textSecondary color
            holder.mMoveEditItemImageViewFavorite.setColorFilter(mContext.getResources().getColor(R.color.colorTextLight));
        }

        if (actionModeActive) {
            holder.mMoveEditActionModeCheckBox.setVisibility(View.VISIBLE);
            holder.mMoveEditActionModeCheckBox.setChecked(currentItem.getActionModeChecked());
        } else {
            holder.mMoveEditActionModeCheckBox.setVisibility(View.GONE);
        }


        AppDatabase checkDB = AppDatabase.getInstance(mContext);

        //setting the backgroundColor:
        int colorCodingMode = checkDB.profileDao().findByID(PreferenceManager.getDefaultSharedPreferences(mContext).getInt("CurrentProfileId", 0)).getColorCodingMoveCardsMode();
        String difficultyColor;
        String spotTypeColor;
        String leftColor = null;
        String rightColor = null;
        try {
            switch (colorCodingMode) {
                case 0:
                    leftColor = "#00000000";
                    rightColor = "#00000000";
                    break;
                case 1:
                    difficultyColor = checkDB.moveStyleOrDifficultyDao().findByID(currentItem.getStyleOrDifficulty()).getColorCoding();
                    leftColor = difficultyColor;
                    rightColor = difficultyColor;
                    break;
                case 2:
                    spotTypeColor = checkDB.moveSpotTypeNeededDao().findByID(currentItem.getSpotTypeNeeded()).getColorCoding();
                    leftColor = spotTypeColor;
                    rightColor = spotTypeColor;
                    break;
                case 3:
                    difficultyColor = checkDB.moveStyleOrDifficultyDao().findByID(currentItem.getStyleOrDifficulty()).getColorCoding();
                    spotTypeColor = checkDB.moveSpotTypeNeededDao().findByID(currentItem.getSpotTypeNeeded()).getColorCoding();
                    leftColor = difficultyColor;
                    rightColor = spotTypeColor;
                    break;
                default:
                    leftColor = null;
                    rightColor = null;
            }
        } catch (NullPointerException e) {
            leftColor = null;
            rightColor = null;
        }
        if (leftColor != null) {
            holder.moveEditBackgroundColorViewLeft.setBackgroundColor(Color.parseColor(leftColor));
        }

        if (rightColor != null) {
            holder.moveEditBackgroundColorViewRight.setBackgroundColor(Color.parseColor(rightColor));
        }


        //Setting the Difficulty and SpotType:
        String currentLanguage = PreferenceManager.getDefaultSharedPreferences(mContext).getString(mContext.getResources().getString(R.string.sharedpref_current_selected_language), "en");
        Move currentMove = currentItem.getMove();

        holder.mTextMoveName.setText(getLocalisedMoveName(currentMove, currentLanguage));
        holder.mTextMoveStyleOrDifficulty.setText(getLocalisedMoveStyleOrDifficultyDescr(checkDB, currentMove.getStyleOrDifficulty(), currentLanguage)); //AppDatabase.getInstance(mContext).moveStyleOrDifficultyDao()
        holder.mTextMoveSpotTypeNeeded.setText(getLocalisedMoveSpotTypeNeededDescr(checkDB, currentMove.getSpotTypeNeeded(), currentLanguage)); //AppDatabase.getInstance(mContext).moveSpotTypeNeededDao()
        //TODO: make more efficient query that gives out COUNT instead of getting all spots and getting their ListSize
        holder.mTextUsedAmount.setText(String.valueOf(checkDB.moveSpotJoinDao().getSpotsForMove(currentMove.getID()).size()));



    }

    public String getLocalisedMoveName(Move move, String language) {
        switch(language) {
            case "en":
                return move.getEnglishName();
            case "de":
                return move.getGermanName();
            default:
                return move.getEnglishName();
        }
    }

    public String getLocalisedMoveStyleOrDifficultyDescr(AppDatabase checkDBin, int diffID, String language) {
        try {
            switch (language) {
                case "en":
                    if (diffID == 99) {
                        return "undefined";
                    }
                    return checkDBin.moveStyleOrDifficultyDao().findByID(diffID).getEnglishDescr();
                case "de":
                    if (diffID == 99) {
                        return "unbestimmt";
                    }
                    return checkDBin.moveStyleOrDifficultyDao().findByID(diffID).getGermanDescr();
                default:
                    if (diffID == 99) {
                        return "undefined";
                    }
                    return checkDBin.moveStyleOrDifficultyDao().findByID(diffID).getEnglishDescr();
            }
        } catch (NullPointerException e) {
            return "error :(";
        }
    }

    public String getLocalisedMoveSpotTypeNeededDescr(AppDatabase checkDBin, int spotTypeID, String language) {
        try {
            switch (language) {
                case "en":
                    if (spotTypeID == 99) {
                        return "undefined";
                    }
                    return checkDBin.moveSpotTypeNeededDao().findByID(spotTypeID).getEnglishDescr();
                case "de":
                    if (spotTypeID == 99) {
                        return "unbestimmt";
                    }
                    return checkDBin.moveSpotTypeNeededDao().findByID(spotTypeID).getGermanDescr();
                default:
                    if (spotTypeID == 99) {
                        return "undefined";
                    }
                    return checkDBin.moveSpotTypeNeededDao().findByID(spotTypeID).getEnglishDescr();
            }
        } catch (NullPointerException e) {
            return "error :(";
        }
    }




    @Override
    public int getItemCount() {
        return mMoveEditList.size();
    }

    public int getPosition() { return position; }

    public void setPosition(int position) {
        this.position = position;
    }

    private void toastMessage (String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
}
