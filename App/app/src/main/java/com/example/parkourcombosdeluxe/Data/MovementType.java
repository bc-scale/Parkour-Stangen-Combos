package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class MovementType {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    String englishDescr;
    String germanDescr;
    String colorCoding;
    Boolean changeable;
    Integer imageResourceID;

    MovementType(String englishDescr, String germanDescr, String colorCoding, Boolean changeable, Integer imageResourceID) {
        this.englishDescr = englishDescr;
        this.germanDescr = germanDescr;
        this.colorCoding = colorCoding;
        this.changeable = changeable;
        this.imageResourceID = imageResourceID;
    }

    @Ignore
    public MovementType(@NonNull String englishDescr, String germanDescr, Boolean changeable) {
        this(englishDescr, germanDescr, null, changeable, null);
    }

    @Ignore
    public MovementType(@NonNull String englishDescr, String germanDescr, Integer imageResourceID) {
        this(englishDescr, germanDescr, null, true, imageResourceID);
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getEnglishDescr() {
        return englishDescr;
    }
    public void setEnglishDescr(String englishDescr) {
        this.englishDescr = englishDescr;
    }

    public String getGermanDescr() {
        return germanDescr;
    }
    public void setGermanDescr(String germanDescr) {
        this.germanDescr = germanDescr;
    }

    public String getColorCoding() {
        return colorCoding;
    }
    public void setColorCoding(String colorCoding) {
        this.colorCoding = colorCoding;
    }

    public Boolean getChangeable() {
        return changeable;
    }
    public void setChangeable(Boolean changeable) {
        this.changeable = changeable;
    }

    public Integer getImageResourceID() {
        return imageResourceID;
    }
    public void setImageResourceID(Integer imageResourceID) {
        this.imageResourceID = imageResourceID;
    }


}