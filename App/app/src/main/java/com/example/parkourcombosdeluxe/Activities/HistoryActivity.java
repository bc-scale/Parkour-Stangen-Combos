package com.example.parkourcombosdeluxe.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.CurrentCombo;
import com.example.parkourcombosdeluxe.Data.CurrentComboDao;
import com.example.parkourcombosdeluxe.Data.History;
import com.example.parkourcombosdeluxe.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends BaseActivity {

    @BindView(R.id.layout_HistoryScroll)
    LinearLayout layout_HistoryScroll;
    @BindView(R.id.history_text_moves_so_far)
    TextView textMovesSoFar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        generateHistory();
    }

    private void generateHistory() {
        List<History> historys = AppDatabase.getInstance(this).historyDao().getAll();
        layout_HistoryScroll.removeAllViews();
        if (historys.size() == 0) {
            generateMessageEmptyHistory();
        } else {
            List<CurrentCombo> checkMovesSoFar = AppDatabase.getInstance(this).currentComboDao().getAll();
            int checkMovesSize = checkMovesSoFar.size();
            if (checkMovesSize > 0) {
                String message = getResources().getString(R.string.history_moves_so_far) + " " + checkMovesSoFar.get(checkMovesSoFar.size() - 1).getID();
                textMovesSoFar.setText(message);
            }

            //go through backwards, to get the newest combo on top
            for (int i = historys.size() - 1; i >= 0; i--) {
                History nextHistory = historys.get(i);
                Boolean done = nextHistory.getDone();

                LinearLayout withTime = new LinearLayout(HistoryActivity.this);
                withTime.setOrientation(LinearLayout.VERTICAL);

                TextView timeAndProfile = new TextView(HistoryActivity.this);
                timeAndProfile.setText(nextHistory.getTimestamp() + "   " + AppDatabase.getInstance(HistoryActivity.this).profileDao().findByID(nextHistory.getProfileID()).getName());
                timeAndProfile.setTextColor(getResources().getColor(R.color.colorTextLight));
                timeAndProfile.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_title));

                LinearLayout entry = new LinearLayout(HistoryActivity.this);
                entry.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));

                ImageView doneCheck = new ImageView(HistoryActivity.this);
                if (done) {
                    doneCheck.setImageDrawable(getDrawable(R.drawable.icon_check_fill_green));
                } else {
                    doneCheck.setImageDrawable(getDrawable(R.drawable.icon_check_empty));
                }

                TextView content = new TextView(HistoryActivity.this);
                content.setText(nextHistory.getContent());
                if (done) {
                    content.setTextColor(getResources().getColor(R.color.colorTextLight));
                } else {
                    content.setTextColor(getResources().getColor(R.color.colorTextLightSoft));
                }
                content.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_descr));

                withTime.addView(timeAndProfile);
                if (nextHistory.getFav()) {
                    LinearLayout withFav = new LinearLayout(HistoryActivity.this);
                    withFav.setOrientation(LinearLayout.VERTICAL);

                    ImageView favHeart = new ImageView(HistoryActivity.this);
                    favHeart.setImageDrawable(getResources().getDrawable(R.drawable.icon_heart_fill_pink));
                    //favHeart.setColorFilter(getResources().getColor(R.color.colorRedFavorite));
                    withFav.addView(doneCheck);
                    withFav.addView(favHeart);
                    entry.addView(withFav);
                } else {
                    entry.addView(doneCheck);
                }
                entry.addView(content);
                withTime.addView(entry);
                layout_HistoryScroll.addView(withTime);
            }
        }
    }

    private void generateMessageEmptyHistory() {
        //TODO clean up
        TextView child = new TextView(HistoryActivity.this);
        child.setWidth(layout_HistoryScroll.getWidth());
        child.setText(getResources().getString(R.string.history_empty_message));
        child.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        child.setTextColor(getResources().getColor(R.color.colorTextLight));
        child.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.main_text_combo_normal));
        layout_HistoryScroll.addView(child);
    }

}