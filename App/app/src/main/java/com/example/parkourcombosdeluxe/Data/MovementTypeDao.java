package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MovementTypeDao {

    @Query("SELECT * FROM movementType")
    List<MovementType> getAll();

    @Query("SELECT * FROM movementType where ID LIKE  :ID")
    MovementType findByID(Integer ID);

    @Insert
    void insertAll(MovementType... movementTypes);

    @Update
    void updateMovementTypes(MovementType... movementTypes);

    @Delete
    void delete(MovementType movementType);

    @Query("DELETE FROM movementType")
    void deleteAll();
}
