#!/bin/awk -E

# Separator for fields: TAB (\t)
BEGIN { FS = "\t" }

{
	# Ignore lines starting with "#"
	if ($1 ~ /^\#/) {next}

    difficulty=$1
    type=$2
    english=$3
    german=$4

    # create moveDao line
    printf "moveDao.insertAll(new Move(%s, %s, \"%s\", \"%s\", false));\n", difficulty, type, english, german
}

# run this script:
