# About the formatting and content of the CSV-files


## Moves
    1.: Column: Difficulty
    2.: Type
    3.: English name
    4.: German name

### Difficulty of moves
In some way sorted by Difficulty, but more of a system to separate Parkour and non-Parkour moves.

 + 1 Basic ('non-Parkour') Movements
 + 2 Parkour Basics
 + 3 Parkour Advanced
 + 4 Parkour Expert
 + 5 Technical Flow Moves (mostly on Rails)
 + 6 Tricking and Freerunning Basics
 + 7 Tricking and Freerunning Advanced
 + 8 Style and 0% Injury Moves

### Type of moves
Trying to put every move into a category. Mainly just the surrounding that it can be done in.     

'Precision' got its own, as they can be done on most things, even on drawn lines.    

'Rail' means you have a usual-rail-height obstacle. Including hip-height walls and bars.

'Rail with some Room to get through' doesn't include walls, as it means there is either just a hip-height bar or at least some space to get through with moves like an underbar. Whether a move needs just a bit of space or more is not further specified.

'Sparda Double Bar and more setup' is a very special spot that inspired the creation of this app with a funky configuration of rails, pillars and pres, so it gets its own type. 'Special' is also mostly included in 'Sparda'.

'High Wall' is any wall with a height that you can comfortably do an arm jump to.

'High Bar' is like a pull up bar, to do laches on.

'Special' is for any other move that needs more than just a floor but doesn't fit into other types. A good example is the move Polio (arm jump to a vertical bar). Warning: moves like Pose or Represent don't go in here, as they don't need anything other than the floor.

 + 1 Ground
 + 2 Precision
 + 3 Rail (/ Hip height Wall)
 + 4 Rail with some Room to get through
 + 5 Sparda Double Bar and more Setup
 + 6 High Wall
 + 7 High bar (to swing on)
 + 8 Special

 #### Relations between types
Some types are included in others. 'A in B' means 'every move of type A is also a move of type B', so choose the type as specific as possible. Current inclusions are:     
 + 0 in Everything
 + 1 in 4
 + 2 in 3, 2 in 4
 + 3 in 4
 